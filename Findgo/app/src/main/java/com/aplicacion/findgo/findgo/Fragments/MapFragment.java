package com.aplicacion.findgo.findgo.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aplicacion.findgo.findgo.Modelos.Alertas;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicolas on 09-12-2016.
 */

public class MapFragment extends Fragment  {

    MapView mMapView;
    private GoogleMap googleMap;

    public List<Alertas> listAlertas = new ArrayList<>();

    JsonObjectRequest arrayRespuesta;

    VolleyS volley;
    RequestQueue requestQueue;

    Thread hiloMostrarAlertas;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mapa, container, false);

        //WebServices
        volley = VolleyS.getInstance(getContext());
        requestQueue = volley.getmRequestQueue();

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                //Mostrar boton para mostrar mi ubicacion

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //User has previously accepted this permission
                    if (ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        googleMap.setMyLocationEnabled(true);
                    }
                } else {
                    //Not in api-23, no need to prompt
                    googleMap.setMyLocationEnabled(true);
                }

                //googleMap.setMyLocationEnabled(true);
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }
                    @Override
                    public View getInfoContents(Marker marker) {

                        Context mContext = getContext();
                        LinearLayout info = new LinearLayout(mContext);
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(mContext);
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTextSize(25);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(mContext);
                        snippet.setTextColor(Color.RED);
                        snippet.setGravity(Gravity.CENTER);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }

                });

            }
        });
        traerAlertas();

        return rootView;
    }

    public void traerAlertas(){

        hiloMostrarAlertas = new Thread(){
            @Override
            public void run(){
                try{
                    //Hacer el metodo

                    arrayRespuesta = new JsonObjectRequest(Request.Method.GET, Constant.URL_TRAER_ALERTAS,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    //Aqui va traer
                                    try {
                                        JSONArray jsonArray = response.getJSONArray("alertas");
                                        String usuario = null;
                                        String fecha = null;
                                        String hora = null;
                                        String fechaFormateada = null;
                                        String horaFormateada = null;

                                        for(int i = 0; i < jsonArray.length(); i++){
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            usuario = jsonObject.getString("usuario");
                                            fecha = jsonObject.getString("fecha");
                                            hora = jsonObject.getString("hora");

                                            horaFormateada = TextUtils.substring (hora,0,5);

                                            SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
                                            SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
                                            try {
                                                fechaFormateada = myFormat.format(fromUser.parse(fecha));
                                            } catch (ParseException e){
                                                e.printStackTrace();
                                            }
                                            LatLng latLng = new LatLng(
                                                    Double.parseDouble(jsonObject.getString("latitud")),
                                                    Double.parseDouble(jsonObject.getString("longitud"))
                                            );
                                            googleMap.addMarker(new MarkerOptions()
                                                    .title(jsonObject.getString("nombre"))
                                                    .snippet(jsonObject.getString("contenido")+"\n"+"Alertado por "+usuario +"\n"+fechaFormateada+" a las "+horaFormateada)
                                                    .position(latLng)
                                            ).showInfoWindow();

                                            CameraPosition alertas = new CameraPosition.Builder()
                                                    .target(latLng)
                                                    .zoom(15)
                                                    .build();

                                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(alertas));
                                        }

                                    }catch (JSONException e){
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(),"Error al traer las alertas, reintente", Toast.LENGTH_LONG).show();
                        }
                    });
                    requestQueue.add(arrayRespuesta);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        hiloMostrarAlertas.start();
    }

    @Override
    public void onResume() {
        traerAlertas();
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        hiloMostrarAlertas.interrupt();
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


}
