package com.aplicacion.findgo.findgo;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.Firebase.EnviarNotificacion;
import com.aplicacion.findgo.findgo.Geolocalizacion.GPSTracker;
import com.aplicacion.findgo.findgo.Modelos.Tipos_alertas;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Agregar_alerta extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<Status> {
    //Componentes
    EditText txt_alerta;
    TextView edt_ubicacion_actual, txt_latitud, txt_longitud;
    TextInputLayout inputLayout;
    Spinner spinner_tipo_alertas;
    Button btn_alertar;
    //Variables globales de la clase
    String contenidoAlerta = null;
    String usuarioGlob = null;
    String id_tipo_alerta = null;
    String tipo_alerta = null;
    String latitudGlob = null;
    String longitudGlob = null;
    String direccionGlob = null;
    //Peticiones
    public VolleyS volley;
    public RequestQueue requestQueue;
    private JSONArray resultTipoAlertas;
    private ArrayList<String> tipoAlertasArray;
    JsonObjectRequest arrayEnvioAlerta;
    //Base de datos interna
    DbHelper db;

    Thread hiloMostrarTiposAlertas;

    //GPSTracker gpsTracker;
    //Todo con ubicacion
    private static final String TAG = Agregar_alerta.class.getSimpleName();
    private static final String LOCATION_KEY = "location-key";
    private static final String ACTIVITY_KEY = "activity-key";

    // Location API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mLastLocation;
    // Códigos de petición
    public static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_CHECK_SETTINGS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_alerta);
        //Inicializar el DB_helper
        db = new DbHelper(this);
        String resp[] = db.obtenerDatosUsuario();
        //Asigno el usuario de la BD interna a la variable global
        usuarioGlob = resp[0];

        //gpsTracker = new GPSTracker(getApplicationContext());

        //Inicializar componentes
        inputLayout = (TextInputLayout) findViewById(R.id.alerta_input_layauot);
        txt_alerta = (EditText) findViewById(R.id.alertas_descripcion);
        txt_latitud = (TextView) findViewById(R.id.alerta_latitud);
        txt_longitud = (TextView) findViewById(R.id.alerta_longitud);
        edt_ubicacion_actual =  (TextView) findViewById(R.id.alerta_ubicacion_actual);
        spinner_tipo_alertas = (Spinner) findViewById(R.id.alertas_categoria);
        btn_alertar = (Button) findViewById(R.id.alerta_boton);
        //WebServices
        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();
        //Llamo los metodos de la clases
        setClickBoton();
        hiloMostrarTiposAlertas = new Thread(){
            @Override
            public void run(){
                try{
                    //Hacer el metodo
                    mostrarTipoAlerta();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        hiloMostrarTiposAlertas.start();

        spinnerListener();

        //Todo para la API de localizacion
        //Establecer punto de entrada a la API
        builGoogleApiClient();
        //Configuracion de las peticiones
        createLocationRequest();
        //Opciones de peticiones
        buildLocationSettingsRequest();
        //Verificar los ajustes
        verificarAjustes();

        updateValuesFromBundle(savedInstanceState);
    }

    @Override
    public void onStop(){
       // hiloMostrarUbicacion.interrupt();
        super.onStop();
    }
    @Override
    protected void onPause() {
        hiloMostrarTiposAlertas.interrupt();
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "El usuario permitió el cambio de ajustes de ubicación.");
                        processLastLocation();
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "El usuario no permitió el cambio de ajustes de ubicación");
                        break;
                }
                break;
        }
    }

    public void setClickBoton(){
        btn_alertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Rescatar todas las weas
                contenidoAlerta = txt_alerta.getText().toString();
                //Aquí llamar metodo para enviar la alerta al WS 1313
                if(txt_alerta.getText().length() > 0){
                    enviarAlerta();
                    btn_alertar.setEnabled(false);
                }else{
                    Snackbar.make(v,"Ingrese descripción para alerta",Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    public void enviarNotificacion(){
       EnviarNotificacion en = new EnviarNotificacion(getApplicationContext() , "Alerta de "+tipo_alerta, usuarioGlob +" alertó " + "\""+contenidoAlerta+"\"" +" en "+direccionGlob ,"Alerta","","");
       en.enviarAlServer();
    }

    public void spinnerListener(){
        spinner_tipo_alertas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Instancia a la clase que llene previamente :3
                Tipos_alertas tip = (Tipos_alertas) parent.getSelectedItem();
                //Rescato el ID del tipo de alerta para mandarlo al WS
                id_tipo_alerta = tip.getId_tipo_alertas();
                tipo_alerta = tip.getNombre();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void mostrarTipoAlerta(){
        //Peticion al WS
         StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL_MOSTRAR_TIPOS_ALERTAS,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Manejo de respuesta, se manda al adaptador y todas las weas xd
                JSONObject j = null;
                try {
                    j = new JSONObject(response);
                    resultTipoAlertas = j.getJSONArray("tipos_alertas");
                    obtenerTiposAlertas(resultTipoAlertas);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error al cargar tipos de alerta "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(stringRequest);
    }

    private void obtenerTiposAlertas(JSONArray j) {
        ArrayList<Tipos_alertas> tiposAlertaList = new ArrayList<>();
        for(int i = 0; i<j.length();i++){
            try{
                JSONObject json = j.getJSONObject(i);
                String id_tipo_alerta;
                String nombre;

                id_tipo_alerta = json.getString("id_tipo_alertas");
                nombre = json.getString("nombre");
                tiposAlertaList.add(new Tipos_alertas(id_tipo_alerta,nombre));

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        ArrayAdapter<Tipos_alertas> adapter = new ArrayAdapter<Tipos_alertas>(this, android.R.layout.simple_spinner_dropdown_item, tiposAlertaList);
        spinner_tipo_alertas.setAdapter(adapter);
    }

    public void enviarAlerta() {
        String contenidoURL = URLEncoder.encode(contenidoAlerta).replace("+", "%20");
        String usuarioURL = URLEncoder.encode(usuarioGlob).replace("+", "%20");
        String latitudURL = URLEncoder.encode(latitudGlob).replace("+", "%20");
        String longitudURL = URLEncoder.encode(longitudGlob).replace("+", "%20");
        String direccionURL = URLEncoder.encode(direccionGlob).replace("+", "%20");

        String cadena = id_tipo_alerta+"/"+contenidoURL+"/"+usuarioURL+"/"+latitudURL+"/"+longitudURL+"/"+direccionURL;
        //Este es POST Tio
        arrayEnvioAlerta = new JsonObjectRequest(Request.Method.POST, Constant.URL_ENVIAR_ALERTA+cadena,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        //la respuesta al enviar
                        String verificacion = response.optString("alertar");
                        if(verificacion.equals("true")) {
                           enviarNotificacion();
                           Toast.makeText(getApplicationContext(), "Alerta publicada correctamente",Toast.LENGTH_LONG).show();
                           finish();
                        }else {
                            Snackbar.make(getWindow().getDecorView().getRootView(), verificacion, Snackbar.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(getWindow().getDecorView().getRootView(), "Error en el servidor, reintente más tarde", Snackbar.LENGTH_LONG).show();
            }
        });
        arrayEnvioAlerta.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(arrayEnvioAlerta);
    }


    //Todo lo que tiene que ver con Google Location ...
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Obtenemos la última ubicación al ser la primera vez
        processLastLocation();
        // Iniciamos las actualizaciones de ubicación
        startLocationUpdates();
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi
                .removeLocationUpdates(mGoogleApiClient, this);
    }

    private void processLastLocation() {
        getLastLocation();
        if (mLastLocation != null) {
            updateLocationUI();
        }
    }

    private void getLastLocation() {
        if (isLocationPermissionGranted()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } else {
            manageDeniedPermission();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Conexión suspendida");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(
                this,
                "Error de conexión con el código:" + connectionResult.getErrorCode(),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Detección de actividad iniciada");

        } else {
            Log.e(TAG, "Error al iniciar/remover la detección de actividad: "
                    + status.getStatusMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, String.format("Nueva ubicación: (%s, %s)",
                location.getLatitude(), location.getLongitude()));
        mLastLocation = location;
        updateLocationUI();
    }

    private synchronized void builGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .enableAutoManage(this, this)
                .build();
    }
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest)
                .setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest()
                .setInterval(1000)
                .setFastestInterval(1000/2)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //Actualiza el UI con la unicacion
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(LOCATION_KEY)) {
                mLastLocation = savedInstanceState.getParcelable(LOCATION_KEY);
                updateLocationUI();
            }
        }
    }
    private void updateLocationUI() {
       // mLatitude.setText(String.valueOf(mLastLocation.getLatitude()));
        //mLongitude.setText(String.valueOf(mLastLocation.getLongitude()));
        direccionGlob = obtenerDireccion(mLastLocation.getLatitude(),mLastLocation.getLongitude());

        edt_ubicacion_actual.setText(direccionGlob);
        latitudGlob = String.valueOf(mLastLocation.getLatitude());
        longitudGlob = String.valueOf(mLastLocation.getLongitude());
        txt_longitud.setText(" Longitud "+longitudGlob);
        txt_latitud.setText("Latitud "+latitudGlob);

      //  edt_ubicacion_actual.setText("Aqui la ubicacion con geocoder");
    }

    private String obtenerDireccion(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                StringBuilder strReturnedAddress = new StringBuilder("");
                String pais = returnedAddress.getCountryName();

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString()+ pais;
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void verificarAjustes() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient, mLocationSettingsRequest
                );
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "Los ajustes de ubicación satisfacen la configuración.");
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            Log.d(TAG, "Los ajustes de ubicación no satisfacen la configuración. " +
                                    "Se mostrará un diálogo de ayuda.");
                            status.startResolutionForResult(
                                    Agregar_alerta.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.d(TAG, "El Intent del diálogo no funcionó.");
                            // Sin operaciones
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.d(TAG, "Los ajustes de ubicación no son apropiados.");
                        break;

                }
            }
        });
    }

    private void startLocationUpdates() {
        if (isLocationPermissionGranted()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else {
            manageDeniedPermission();
        }
    }

    private void manageDeniedPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Aquí muestras confirmación explicativa al usuario
            // por si rechazó los permisos anteriormente
        } else {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    private boolean isLocationPermissionGranted() {
        int permission = ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        return permission == PackageManager.PERMISSION_GRANTED;
    }
}
