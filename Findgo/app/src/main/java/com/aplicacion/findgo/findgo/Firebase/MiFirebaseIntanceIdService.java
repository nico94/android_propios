package com.aplicacion.findgo.findgo.Firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.aplicacion.findgo.findgo.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Nicolas on 09-12-2016.
 */

public class MiFirebaseIntanceIdService extends FirebaseInstanceIdService {

    public static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN, token);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.FCM_TOKEN),token);
        editor.commit();
    }
}
