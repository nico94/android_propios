package com.aplicacion.findgo.findgo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.aplicacion.findgo.findgo.Adaptadores.Adaptador_Comentarios;
import com.aplicacion.findgo.findgo.Adaptadores.Adaptador_Detalles;
import com.aplicacion.findgo.findgo.Fragments.Fragment_Detalle_publicacion;
import com.aplicacion.findgo.findgo.Modelos.Publicaciones;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.android.volley.Request.*;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_DATOS_PUBLICACION;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_IMAGENES;

public class Detalle_publicacion extends AppCompatActivity {

    private Adaptador_Detalles adaptador_detalles;
    private ViewPager viewPager;

    final int[] iconos = new int[]{
            R.drawable.detalle_publicacion,
            R.drawable.comentarios_publicacion,
            R.drawable.ubicacion_publicacion
    };

    String id_post = null;
    String tituloGlob = null;
    String usuario_publicacion = null;
    String latitudGlob = null;
    String longitudGlob = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_publicacion);

       // Toolbar toolbar =(Toolbar) findViewById(R.id.detalle_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.detalle_Appbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.detalle_TabLayout);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        viewPager = (ViewPager) findViewById(R.id.detalle_view_pager);
        adaptador_detalles = new Adaptador_Detalles(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adaptador_detalles);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(iconos[0]);
        tabLayout.getTabAt(1).setIcon(iconos[1]);
        tabLayout.getTabAt(2).setIcon(iconos[2]);

        //Rescatar lo que viene de otra actividad
        Intent intent = this.getIntent();
        id_post = intent.getStringExtra("id_post");
        usuario_publicacion = intent.getStringExtra("usuario");
        latitudGlob = intent.getStringExtra("latitud");
        longitudGlob = intent.getStringExtra("longitud");
        tituloGlob = intent.getStringExtra("titulo");
       // Toast.makeText(getApplicationContext(),"LAT: "+latitudGlob+" LONG: "+longitudGlob,Toast.LENGTH_LONG).show();
    }

    public String getId_post() {
        return id_post;
    }
    public String getUsuario_publicacion(){
        return usuario_publicacion;
    }

    public String getLatitudGlob(){
        return latitudGlob;
    }
    public String getLongitudGlob(){
        return longitudGlob;
    }
    public String getTituloGlob(){
        return tituloGlob;
    }
}
