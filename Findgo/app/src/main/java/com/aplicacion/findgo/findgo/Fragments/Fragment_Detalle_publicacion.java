package com.aplicacion.findgo.findgo.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.aplicacion.findgo.findgo.Detalle_publicacion;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_DATOS_PUBLICACION;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_IMAGENES;

public class Fragment_Detalle_publicacion extends Fragment {

    //Controles
    TextView txt_titulo, txt_contenido, txt_direccion, txt_usuario,txt_categoria,txt_fecha,txt_hora, txt_nombreAn, txt_telefonoAn, txt_emailAn;
    NetworkImageView img_publicacion;
    CircleImageView imagenUsuario;
    ImageLoader imageLoader;
    CardView card_usuario_detalle_publicacion;
    //WS
    public VolleyS volley;
    public RequestQueue requestQueue;
    //Declaro arreglo del Json
    JsonObjectRequest array;
    //Declaro hilo
    Thread hiloMostrarDetalle ;

    String usuarioPublicacion = null;
    String ubicacionGlob = null;
    String latitudGlog = null;
    String longitudGlob = null;

    String id_post= "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detalle_publicacion, container, false);
        //Traigo el id post desde la actividad principal
        Detalle_publicacion activity = (Detalle_publicacion) getActivity();
        id_post = activity.getId_post();

        txt_titulo = (TextView) rootView.findViewById(R.id.titulo_detalle);
        txt_contenido = (TextView) rootView.findViewById(R.id.contenido_detalle);
        txt_direccion = (TextView) rootView.findViewById(R.id.detalle_ubicacion);
        txt_categoria = (TextView) rootView.findViewById(R.id.detalle_categoria);
        txt_usuario = (TextView) rootView.findViewById(R.id.detalle_usuario);
        txt_fecha = (TextView) rootView.findViewById(R.id.detalle_fecha);
        txt_hora = (TextView) rootView.findViewById(R.id.detalle_hora);
        txt_nombreAn = (TextView) rootView.findViewById(R.id.anunciante_nombre);
        txt_telefonoAn = (TextView) rootView.findViewById(R.id.anunciante_telefono);
        txt_emailAn = (TextView) rootView.findViewById(R.id.anunciante_email);
        card_usuario_detalle_publicacion = (CardView) rootView.findViewById(R.id.card_usuario_detalle_publicacion);
        img_publicacion = (NetworkImageView) rootView.findViewById(R.id.imagen_detalle);
        imagenUsuario = (CircleImageView) rootView.findViewById(R.id.foto_perfil_detalle_publicacion);

        //Peticiones al ws
        volley = VolleyS.getInstance(getActivity().getApplication());
        requestQueue = volley.getmRequestQueue();
        imageLoader = VolleyS.getInstance(getActivity()).getImageLoader();

        //Nuevo hilo para mostrar detalle
        hiloMostrarDetalle = new Thread(){
            @Override
            public void run(){
                try{
                    mostrar_detalle(id_post);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        //Si el id post  viene vacio se cierra la actividad padre
        if(id_post != null){
            hiloMostrarDetalle.start();
        }else{
            getActivity().finish();
        }
        clickName();

        return rootView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onDestroy() {
        System.out.println("Se para el hilo on destroy");
        hiloMostrarDetalle.interrupt();
        super.onDestroy();
    }

    public void clickName(){
        txt_nombreAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                contactIntent
                        .putExtra(ContactsContract.Intents.Insert.NAME,txt_nombreAn.getText())
                        .putExtra(ContactsContract.Intents.Insert.EMAIL, txt_emailAn.getText())
                        .putExtra(ContactsContract.Intents.Insert.PHONE, txt_telefonoAn.getText());

                startActivityForResult(contactIntent, 1);

            }
        });
    }


    public void mostrar_detalle(String id){
        //Progress Bar
       // final ProgressDialog cargando = new ProgressDialog(getActivity().getApplication());
        //cargando.setMessage("Cargando detalle de la publicación, espere...");
        //cargando.show();

        array = new JsonObjectRequest(Request.Method.GET, URL_DATOS_PUBLICACION + id, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //cargando.dismiss();
                //Mandar los datos a los TextView
                try {
                    JSONArray respuesta = response.optJSONArray("datos");
                    String categoriaR = "";
                    String tituloR = "";
                    String contenidoR = "";
                    String imagenR = "";
                    String fechaR ="";
                    String horaR ="";
                    String usuarioR = "";
                    String latitudR = "";
                    String longitudR = "";
                    String ubicacionR = "";
                    String nombreUser = "";
                    String apellidoUser = "";
                    String telefonoUser = "";
                    String direccionUser = "";
                    String emailUser = "";
                    String imagenUser = "";

                    for (int i = 0; i<respuesta.length();i++) {
                        JSONObject json = respuesta.getJSONObject(i);
                        categoriaR = json.optString("nombre_categoria");
                        tituloR = json.optString("post");
                        contenidoR = json.optString("contenido");
                        imagenR = json.optString("imagen");
                        fechaR = json.optString("fecha");
                        horaR = json.optString("hora");
                        usuarioR = json.optString("usuario");
                        latitudR = json.optString("latitud");
                        longitudR = json.optString("longitud");
                        ubicacionR = json.optString("ubicacion");
                        nombreUser = json.optString("nombres");
                        apellidoUser = json.optString("apellidos");
                        telefonoUser = json.optString("telefono");
                        direccionUser = json.optString("direccion");
                        emailUser = json.optString("email");
                        imagenUser = json.optString("imagen_user");
                    }
                    //Toast.makeText(getActivity(),"LongitudWS "+longitudR+" latWS "+latitudR,Toast.LENGTH_LONG).show();
                    usuarioPublicacion = usuarioR;
                    latitudGlog = latitudR;
                    longitudGlob = longitudR;
                    getUsuario(usuarioPublicacion);

                    mostrarDatos(categoriaR,tituloR,contenidoR,imagenR,fechaR,horaR,usuarioR,latitudR,longitudR,ubicacionR
                            ,nombreUser,apellidoUser,telefonoUser,direccionUser,emailUser,imagenUser);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // cargando.cancel();
               Toast.makeText(getActivity(),"No hay respuesta del servidor, reintente",Toast.LENGTH_LONG).show();

                //Snackbar.make(getView(), "Error al cargar, reintente", Snackbar.LENGTH_INDEFINITE).show();
                getActivity().finish();
            }
        });
        //Agregar la petiicon al ws
        requestQueue.add(array);
    }

    public void mostrarDatos(String categoria,String titulo, String contenido, String imagen, String fecha, String hora,String usuario, String latitud ,String longitudR, String ubicacion,
                             String nombreUser, String apellidoUser, String telefonoUser, String direccionUser, String emailUser, String imagenUser){
        String imagenUrl = URL_IMAGENES+imagenUser;
        String fechaFormateada = "";
        String horaFormateada = TextUtils.substring (hora,0,5);
        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            fechaFormateada = myFormat.format(fromUser.parse(fecha));
        } catch (ParseException e){
            e.printStackTrace();
        }

        imageLoader.get(URL_IMAGENES+imagen, imageLoader.getImageListener(img_publicacion, android.R.drawable.ic_menu_camera, android.R.drawable.ic_dialog_alert));
        Picasso.with(getActivity())
                .load(imagenUrl)
                .placeholder(R.drawable.usuario_card)
                .error(android.R.drawable.ic_dialog_alert)
                .fit()
                .centerCrop()
                .into(imagenUsuario);
        img_publicacion.setImageUrl(URL_IMAGENES+imagen,imageLoader);
        img_publicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aqui hacerle zoom
            }
        });
        txt_direccion.setText(ubicacion);
        txt_titulo.setText(titulo);
        txt_contenido.setText(contenido);
        txt_usuario.setText(usuario);
        txt_categoria.setText(categoria);
        txt_fecha.setText(fechaFormateada);
        txt_hora.setText("a las "+horaFormateada+" horas");
        txt_nombreAn.setText(nombreUser+" "+apellidoUser);
        txt_telefonoAn.setText(telefonoUser);
        txt_emailAn.setText(emailUser);
    }

    public String getUsuario(String usuariodueno) {
        System.out.println("usuario dueño "+usuariodueno);
        return usuarioPublicacion;
    }

    public String getLatitudGlog(){
        return latitudGlog;
    }
    public String getLongitudGlob(){
        return longitudGlob;
    }

}
