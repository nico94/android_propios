package com.aplicacion.findgo.findgo.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.aplicacion.findgo.findgo.Modelos.Comentario;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nicolas on 28-11-2016.
 */

public class Adaptador_Comentarios extends RecyclerView.Adapter<Adaptador_Comentarios.ViewHolder>{

    private Context context;

    //Lista de comentarios
    List<Comentario> comentarios1;
    Animation animationFadeIn;

    public Adaptador_Comentarios(List<Comentario> comentarios2, Context context){
        super();
        this.comentarios1 = comentarios2;
        this.context = context;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tarjeta_comentario, parent, false);
        ViewHolder ViewHolder = new ViewHolder(v);
        animationFadeIn = (Animation) AnimationUtils.loadAnimation(parent.getContext(), R.anim.fadein);
        v.setAnimation(animationFadeIn);

        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){
        final Comentario comentario = comentarios1.get(position);

        String fechaWS = comentario.getFecha();
        String horaWS = comentario.getHora();
        String fechaFormateada = null;
        String horaFormateada = TextUtils.substring(horaWS,0,5);

        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            fechaFormateada = myFormat.format(fromUser.parse(fechaWS));
        } catch (ParseException e){
            e.printStackTrace();
        }

        String imagenUrl = Constant.URL_IMAGENES+comentario.getUserImage();
        Picasso.with(context).load(imagenUrl)
                .placeholder(R.drawable.usuario_card)
                .error(R.drawable.usuario_card)
                .fit()
                .centerCrop()
                .into(holder.imagenUsuarioCom);
        holder.usuarioCom.setText(comentario.getUsuario());
        holder.comentarioCom.setText(comentario.getComentario());
        holder.fechaCom.setText(fechaFormateada);
        holder.horaCom.setText("a las "+horaFormateada+" horas");
    }

    @Override
    public int getItemCount(){
        return comentarios1.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView id_postCom;
        public TextView usuarioCom;
        public TextView comentarioCom;
        public TextView fechaCom;
        public TextView horaCom;
        public CircleImageView imagenUsuarioCom;

        public ViewHolder(final View itemView){
            super(itemView);

            usuarioCom = (TextView) itemView.findViewById(R.id.comentario_usuario);
            comentarioCom = (TextView) itemView.findViewById(R.id.comentario);
            fechaCom = (TextView) itemView.findViewById(R.id.comentario_fecha);
            horaCom = (TextView) itemView.findViewById(R.id.comentario_hora);
            imagenUsuarioCom = (CircleImageView) itemView.findViewById(R.id.comentario_imagen_usuario);
        }
    }

}
