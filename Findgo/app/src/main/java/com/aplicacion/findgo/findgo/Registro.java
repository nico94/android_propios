package com.aplicacion.findgo.findgo;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_REGISTRO;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_VALIDA_USUARIO;

public class Registro extends AppCompatActivity implements View.OnClickListener {

    Button btn_registro2;
    EditText edt_usuario, edt_password, edt_password2, edt_nombre, edt_apellido, edt_telefono, edt_direccion, edt_email;
    TextInputLayout input_email, input_usuario;
    public VolleyS volley;
    public RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Buscar los ID
        btn_registro2 = (Button) findViewById(R.id.boton_registro2);
        edt_usuario = (EditText) findViewById(R.id.edt_usuarioR);
        edt_password = (EditText) findViewById(R.id.edt_passwordR1);
        edt_password2 = (EditText) findViewById(R.id.edt_passwordR2);
        edt_nombre = (EditText) findViewById(R.id.edt_nombreR);
        edt_apellido = (EditText) findViewById(R.id.edt_apellidoR);
        edt_telefono = (EditText) findViewById(R.id.edt_telefono);
        edt_direccion = (EditText) findViewById(R.id.edt_direccion);
        edt_email = (EditText) findViewById(R.id.edt_correo);
        input_email = (TextInputLayout) findViewById(R.id.input_email);
        input_usuario = (TextInputLayout) findViewById(R.id.input_usuario);
        edt_email.addTextChangedListener(new MiVisor(edt_email));
        edt_usuario.addTextChangedListener(new MiVisor(edt_usuario));
        //Gatillar el evento click
        btn_registro2.setOnClickListener(this);
        //Peticiones al ws
        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();
    }

    //Casos al hacer click
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.boton_registro2:
                //Rescatar los strings de los EDT
                String usuarioR = edt_usuario.getText().toString();
                String passwordR = edt_password.getText().toString();
                String passwordR2 = edt_password2.getText().toString();
                String nombreR = edt_nombre.getText().toString();
                String apellidoR = edt_apellido.getText().toString();
                String telefonoR = edt_telefono.getText().toString();
                String direccionR = edt_direccion.getText().toString();
                String emailR = edt_email.getText().toString();
                String cadena;

                //Valida los EditText
                if (edt_usuario.getText().length() == 0) {
                    Snackbar.make(v, "Usuario requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_nombre.getText().length() == 0) {
                    Snackbar.make(v, "Nombre requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_apellido.getText().length() == 0) {
                    Snackbar.make(v, "Apellido requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_direccion.getText().length() == 0) {
                    Snackbar.make(v, "Dirección requerida", Snackbar.LENGTH_LONG).show();
                } else if (edt_telefono.getText().length() == 0) {
                    Snackbar.make(v, "Teléfono requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_email.getText().length() == 0 || !validarCorreo()) {
                    Snackbar.make(v, "Email requerido", Snackbar.LENGTH_LONG).show();
                } else {
                    if (passwordR.equalsIgnoreCase(passwordR2)) {
                       //Convertir los datos a URL para los caracteres especiales (á , : ; etc)
                        String usuarioURL = URLEncoder.encode(usuarioR).replace("+", "%20");
                        String passwordURL = URLEncoder.encode(passwordR).replace("+", "%20");
                        String nombreURL = URLEncoder.encode(nombreR).replace("+", "%20");
                        String apellidoURL = URLEncoder.encode(apellidoR).replace("+", "%20");
                        String telefonoURL = URLEncoder.encode(telefonoR).replace("+", "%20");
                        String direccionURL = URLEncoder.encode(direccionR).replace("+", "%20");
                        String emailURL = URLEncoder.encode(emailR).replace("+", "%20");
                        cadena = usuarioURL + "/" + passwordURL + "/" + nombreURL + "/" + apellidoURL + "/" + telefonoURL + "/" + direccionURL + "/" + emailURL;
                        hacerRegistro(cadena);
                    } else {
                        Snackbar.make(v, "Las contraseñas no coinciden", Snackbar.LENGTH_LONG).show();
                        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                        edt_password.startAnimation(shake);
                        edt_password2.startAnimation(shake);
                    }
                }
        }
    }

    public void hacerRegistro(String cadena) {
        //Toast.makeText(getApplicationContext(), "cadena en ws "+cadena, Toast.LENGTH_SHORT).show();
        StringRequest request = new StringRequest(Request.Method.POST, URL_REGISTRO + cadena, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject respuesta = new JSONObject(response);
                    String verificacion = respuesta.optString("respuesta");
                    if (verificacion.toString().equals("Usuario registrado")) {
                        Toast.makeText(getApplicationContext(), "Usuario registrado exitosamente, bienvenido a Findgo!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        //Error en registrar
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Usuario no registrado, reintente más tarde", Snackbar.LENGTH_INDEFINITE).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error al registrar: " + error, Toast.LENGTH_SHORT).show();
                //Al ser error Reintentar
                Snackbar.make(getWindow().getDecorView().getRootView(), "No hay respuesta del servidor, reintente mas tarde", Snackbar.LENGTH_INDEFINITE).show();
            }
        });
        requestQueue.add(request);
    }



    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private class MiVisor implements TextWatcher {
        private View view;

        private MiVisor(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edt_correo:
                    validarCorreo();
                    break;
                case R.id.edt_usuarioR:
                   validarUsuario();
                   break;
            }
        }
    }

    private boolean validarCorreo() {
        String email = edt_email.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            input_email.setError("ingrese un email válido");
            //requestFocus(input_email);
            return false;
        } else {
            input_email.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void validarUsuario() {
        //Hacer una peticion a ws
        String usuario = edt_usuario.getText().toString();
        if (!usuario.isEmpty()) {
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_VALIDA_USUARIO + usuario, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String respuesta = "";
                    respuesta = response.optString("existe");
                    if (respuesta.equals("true")) {
                        input_email.setErrorEnabled(true);
                        input_usuario.setError("este usuario ya existe");

                        btn_registro2.setEnabled(false);
                    }else if (respuesta.equals("false")){
                        input_usuario.setErrorEnabled(false);
                        btn_registro2.setEnabled(true);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            requestQueue.add(request);
        }
    }
}