package com.aplicacion.findgo.findgo.Fragments;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aplicacion.findgo.findgo.Geolocalizacion.GPSTracker;
import com.aplicacion.findgo.findgo.R;

/**
 * Created by Nicolas on 08-09-2016.
 */
public class Pagina2 extends Fragment {

    String longitudTXT;
    String latitudTXT;
    TextView longitud;
    TextView latitud;
    TextView direccionTEX;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  "Inflamos" el archivo XML correspondiente a esta sección.
        View rootView = inflater.inflate(R.layout.fragment_pagina2,container,false);


        longitud = (TextView) rootView.findViewById(R.id.longitud);
        latitud = (TextView) rootView.findViewById(R.id.latitud);
        direccionTEX = (TextView) rootView.findViewById(R.id.direccion);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }

}


