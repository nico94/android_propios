package com.aplicacion.findgo.findgo.Fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aplicacion.findgo.findgo.Adaptadores.Adaptador_Comentarios;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.Detalle_publicacion;
import com.aplicacion.findgo.findgo.Firebase.EnviarNotificacion;
import com.aplicacion.findgo.findgo.Modelos.Comentario;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_TRAER_COMENTARIOS;

public class Fragment_Comentarios extends Fragment {

    //Declaracion de variables
    public List<Comentario> listComentario = new ArrayList<>();

    //Components
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    Adaptador_Comentarios adaptador_comentarios = new Adaptador_Comentarios(listComentario,getContext());
    CircleButton boton_comentar;
    EditText edt_comentar;
    TextView txt_estado;

    //DB SQLite
    DbHelper db;
    String resp[] = null;

    SwipeRefreshLayout swipeRefreshLayout;
    //WS
    public VolleyS volley;
    public RequestQueue requestQueue;
    //Variables
    String id_post = null;
    String usuario_publicacion = null;
    String usuarioGlob = null;
    String comentarioGlob = null;

    JsonObjectRequest array, arrayComentarios;
    Thread hiloMostrarComentarios;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        //Vista root del fragment
        View rootView = inflater.inflate(R.layout.fragment_comentarios,container,false);
        listComentario = new ArrayList<>();

        Detalle_publicacion activity = (Detalle_publicacion) getActivity();
        id_post = activity.getId_post();
        usuario_publicacion = activity.getUsuario_publicacion();

        boton_comentar = (CircleButton) rootView.findViewById(R.id.boton_comentar);
        edt_comentar = (EditText) rootView.findViewById(R.id.edt_comentar);
        txt_estado = (TextView) rootView.findViewById(R.id.info_comentarios);

        db = new DbHelper(getActivity());
        resp = db.obtenerDatosUsuario();

        usuarioGlob = resp[0];

        //ReciclerView
        recyclerView = (RecyclerView) rootView.findViewById(R.id.comentario_reciclador);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adaptador_comentarios);

        //Inicializar el volley
        volley = VolleyS.getInstance(getActivity().getApplication());
        requestQueue = volley.getmRequestQueue();

        //Hacer un hilo secundario que cargue comentarios
        hiloMostrarComentarios = new Thread(){
          @Override
            public void run(){
              try{
                   //Hacer el metodo
                    mostrarComentarios(id_post);
                }catch (Exception e){
                  e.printStackTrace();
              }
          }
        };
        hiloMostrarComentarios.start();

        //Swipe Refresh
        swipeRefreshLayout =(SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshComentarios);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mostrarComentarios(id_post);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        //metodo del click en el boton comentar
        botonComentar();
        //Retornar el rootView al crear
        return rootView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy(){
        hiloMostrarComentarios.interrupt();
        super.onDestroy();
    }

    public void botonComentar(){
        boton_comentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comentario = edt_comentar.getText().toString();
                String comentarioURL = URLEncoder.encode(comentario).replace("+","%20");
                String usuarioURL = URLEncoder.encode(resp[0]).replace("+","%20");
                //Mandar datos al ws
                comentarioGlob = comentario;
                hacerComentario(id_post,usuarioURL,comentarioURL);
                boton_comentar.setEnabled(false);

            }
        });
    }

    public void enviarNotificacion(){
        if (usuario_publicacion.equals(usuarioGlob) == false ){
            System.out.println("usuario dueño "+usuario_publicacion+" post comentado: "+id_post + " Comentario "+comentarioGlob);
            EnviarNotificacion en = new EnviarNotificacion(getContext(),"¡"+usuario_publicacion+"! han comenado tu publicación",usuarioGlob +" comentó "+"\""+comentarioGlob+"\"","Comentario",usuario_publicacion,id_post);
            en.enviarAlServer();
        }
    }

    public void hacerComentario(final String id_post, String usuario, String comentario){
        String cadena = id_post+"/"+usuario+"/"+comentario;
        //Enviar al WS
        if(comentario.length()!=0){
            arrayComentarios = new JsonObjectRequest(Request.Method.POST, Constant.URL_AGREGAR_COMENTARIO+cadena, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    String verificacion = response.optString("comentar");
                    if(verificacion.equals("true")){
                        edt_comentar.setText(null);
                        enviarNotificacion();
                        mostrarComentarios(id_post);
                        boton_comentar.setEnabled(true);
                    }else{
                        Snackbar.make(getView(), "Error al enviar el comentario", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar.make(getView(), "Error en respuesta del servidor, reintente", Snackbar.LENGTH_INDEFINITE).show();
                }
            });
            requestQueue.add(arrayComentarios);
        }else{
            //Toast.makeText(getActivity(),"No podi mandar un comentario vacio",Toast.LENGTH_LONG).show();
            Snackbar.make(getView(), "Ingrese un comentario", Snackbar.LENGTH_LONG).show();
        }
    }

    public void mostrarComentarios(String id_publicacion){
        listComentario = new ArrayList<>();
        array = new JsonObjectRequest(Request.Method.GET, URL_TRAER_COMENTARIOS + id_publicacion, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //cargando.dismiss();
                try {
                    JSONArray json = response.getJSONArray("comentarios");
//                    Toast.makeText(getActivity(), json.toString(), Toast.LENGTH_LONG).show();
                    parsearDatos(json);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError){
                //Servidor no responde, mostrar Snackbar infinito con boton reintentar presionar ejecuta metodo mostrarPublicaciones()
                //cargando.cancel();
                Snackbar.make(getView(), "No hay respuesta del servidor", Snackbar.LENGTH_INDEFINITE).show();
                getActivity().finish();
            }
        });
        array.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(array);
    }

    private void parsearDatos(JSONArray response){
        for (int i = 0; i<response.length();i++){
            Comentario comen = new Comentario();
            JSONObject json = null;
            try{
                json = response.getJSONObject(i);
                comen.setId_post(json.optString("id_post"));
                comen.setUsuario(json.optString("usuario"));
                comen.setComentario(json.optString("comentario"));
                comen.setFecha(json.optString("fecha_comentario"));
                comen.setHora(json.optString("hora_comentario"));
                comen.setUserImage(json.optString("imagen_user"));

            }catch (JSONException e){
                e.printStackTrace();
            }
            listComentario.add(comen);
        }
        //Inicializar el adaptador
        adapter = new Adaptador_Comentarios(listComentario,getContext());
        //Agregar el adapter al recycler
        recyclerView.setAdapter(adapter);
    }
}
