package com.aplicacion.findgo.findgo;

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.Modelos.Categorias;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_SUBIDA;

public class Agregar_publicacion extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<Status>{

    Toolbar toolbar;
    //Ws
    VolleyS volley;
    RequestQueue requestQueue;
    JsonObjectRequest array;
    private JSONArray resultCategorias;
    private ArrayList<String> categoriasArray;

    //Componentes
    EditText edt_titulo, edt_contenido;
    ImageView img_publicacion;
    TextView direccion_archivo;
    Spinner spinnerCategorias;
    Button boton_publicar;
    CheckBox chk_ubicacion;

    //Todo lo que mandare a publicar
    String tituloPub=null;
    String contenidoPub=null;
    String id_categoriaPub=null;
    String longitudPub=null;
    String latitudPub=null;
    String direccionPub=null;
    String imagenPub=null;
    String usuarioPub =null;

    //Bitmap to get image from gallery
    private Bitmap bitmap;
    private Uri filePath;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //DB SQLite
    DbHelper db;

    Thread hiloCargarCategorias;

    //GPSTracker gpsTracker;

    //Seleccionador de imagen
    public static  final int SELECCION_IMAGEN = 1;
    public static  final int CAMARA_IMAGEN = 2;

    @RequiresApi(api = Build.VERSION_CODES.N)

    //Todo con ubicacion
    private static final String TAG = Agregar_alerta.class.getSimpleName();
    private static final String LOCATION_KEY = "location-key";
    private static final String ACTIVITY_KEY = "activity-key";

    // Location API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mLastLocation;
    // Códigos de petición
    public static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_CHECK_SETTINGS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_publicacion);

        db = new DbHelper(this);
        String resp[] = db.obtenerDatosUsuario();

        //gpsTracker = new GPSTracker(getApplicationContext());

        //Inicializar componentes
        edt_titulo = (EditText) findViewById(R.id.titulo_agregar_post);
        edt_contenido = (EditText) findViewById(R.id.contenido_agregar_post);
        img_publicacion = (AppCompatImageView) findViewById(R.id.imagen_agrega_publicacion);
        img_publicacion.setImageResource(R.drawable.nofoto);
        direccion_archivo = (TextView) findViewById(R.id.direccion_archivo);
        spinnerCategorias = (Spinner) findViewById(R.id.spinner_categorias);
        chk_ubicacion = (CheckBox) findViewById(R.id.check_ubicacion);
       // toolbar = (Toolbar) findViewById(R.id.toolbar_1);
        boton_publicar = (Button) findViewById(R.id.boton_publicar);
        //Inicializar el arreglo
        categoriasArray = new ArrayList<String>();
        //Boton setearle el Click
        botonPublicarClick();

        iamgenClick();
        //Se prendio esta mierda! XD
        //Toolbar
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setTitle("");

        //Helper DB
        db = new DbHelper(this);
        usuarioPub = resp[0];

        //WebServices
        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();

        hiloCargarCategorias = new Thread(){
            @Override
            public void run(){
                try{
                    //Hacer el metodo
                    cargarCategorias();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        hiloCargarCategorias.start();

        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        spinnerListener();
                    }
                }
        ).start();

        //Todo para la API de localizacion
        //Establecer punto de entrada a la API
        builGoogleApiClient();
        //Configuracion de las peticiones
        createLocationRequest();
        //Opciones de peticiones
        buildLocationSettingsRequest();
        //Verificar los ajustes
        verificarAjustes();

        updateValuesFromBundle(savedInstanceState);
    }

    private void iamgenClick() {
        img_publicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                elegirImagen();
            }
        });
    }

    @Override
    public void onStop(){
        hiloCargarCategorias.interrupt();
        super.onStop();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_agregar_pub, menu);
        return true;
    } */

    /*@Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.galeria_iamgen:

                return true;
            case R.id.tomar_iamgen:
                tomarImagen();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    } */

    public void botonPublicarClick(){
        boton_publicar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                tituloPub = edt_titulo.getText().toString();
                contenidoPub = edt_contenido.getText().toString();
                //Transformar datos a URL
                if(tituloPub.length() == 0 ){
                    Snackbar.make(v, "Ingrese título", Snackbar.LENGTH_LONG).show();
                }else if (contenidoPub.length() == 0){
                    Snackbar.make(v, "Ingrese un contenido", Snackbar.LENGTH_LONG).show();
                }else if(imagenPub == null){
                    Snackbar.make(v, "No ha seleccionado ninguna imagen", Snackbar.LENGTH_LONG).show();
                }else if (usuarioPub.length() == 0){
                    Snackbar.make(v, "No se pudo reconocer el usuario", Snackbar.LENGTH_LONG).show();
                }else{
                    String tituloURL = URLEncoder.encode(tituloPub).replace("+","%20");
                    String contenidoURL = URLEncoder.encode(contenidoPub).replace("+","%20");
                    String imagenURL = URLEncoder.encode(imagenPub).replace("+","%20");
                    String usuarioURL = URLEncoder.encode(usuarioPub).replace("+","%20");
                    String direccionURL = URLEncoder.encode(direccionPub).replace("+","%20");
                    String latitudURL = URLEncoder.encode(latitudPub).replace("+","%20");
                    String longitudURL = URLEncoder.encode(longitudPub).replace("+","%20");

                    //Verificar que las variables no vengan nulas
                    if ((id_categoriaPub.equals(null) && tituloURL.equals(null) && contenidoURL.equals(null)
                            && imagenURL.equals(null) && usuarioURL.equals(null) && direccionURL.equals(null) && latitudURL.equals(null) && longitudURL.equals(null)) == false){
                        if(chk_ubicacion.isChecked()){
                            enviarPublicacion(id_categoriaPub,tituloURL,contenidoURL,imagenURL,usuarioURL,direccionURL,latitudURL,longitudURL);
                            boton_publicar.setEnabled(false);
                        }else{
                            enviarPublicacion(id_categoriaPub,tituloURL,contenidoURL,imagenURL,usuarioURL,"null","null","null");
                            boton_publicar.setEnabled(false);
                        }
                    }else{
                        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Vienen datos sin rellenar", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }
            }
        });
    }

    public void spinnerListener() {
        spinnerCategorias.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Categorias categorias = (Categorias) parent.getSelectedItem();
                //Rescato desde el POJOS, el id_categoria que se guardo del WS
                id_categoriaPub = categorias.getId_categoria();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Si no hay nada seleccionado
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case SELECCION_IMAGEN:
                    filePath = data.getData();
                    if(null != filePath) {
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
                            String path = getPath(filePath);
                            //System.out.println(path);
                            direccion_archivo.setText("ruta: "+path.toString());
                            String nombreImagen = path.substring(path.lastIndexOf("/")+1);
                            imagenPub = nombreImagen;
                            img_publicacion.setImageBitmap(bitmap);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return;
                case CAMARA_IMAGEN:
                    bitmap = (Bitmap) data.getExtras().get("data");
                    img_publicacion.setScaleType(ImageView.ScaleType.CENTER);
                    img_publicacion.setImageBitmap(bitmap);
                    return;
            }
        }
    }

    public void uploadMultiPart(){
        String name = imagenPub;
        String path = getPath(filePath);
        try{
            String uploadId = UUID.randomUUID().toString();
            Toast.makeText(getApplicationContext(),"Subiendo imagen, espere...",Toast.LENGTH_LONG).show();
            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, URL_SUBIDA)
                    .addFileToUpload(path, "image") //Adding file
                    .addParameter("name", name) //Adding text parameter to the request
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(3)
                    .startUpload(); //Starting the upload
        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    //Obtener el path desde la URI
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    //Intent para abrir la camara
    public void tomarImagen(){
        Intent intentCamara = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intentCamara, CAMARA_IMAGEN);
    }

    public void elegirImagen(){
               Intent intent= new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Seleccione una imagen"),SELECCION_IMAGEN);
    }

    //Enviar publicacion al WS
    public void enviarPublicacion(String id_categoria, String titulo, String contenido, String imagen, String usuario, String id_ubicacion, String latitud, String longitud){
        String cadena = id_categoria+"/"+titulo+"/"+contenido+"/"+imagen+"/"+usuario+"/"+id_ubicacion+"/"+latitud+"/"+longitud;
        array = new JsonObjectRequest(Request.Method.POST, Constant.URL_AGREGAR_PUBLICACION+cadena,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Manejar respuesta
                String verificacion = response.optString("agrega");
                if(verificacion.equals("true")){
                    uploadMultiPart();
                    Toast.makeText(getApplicationContext(),"Publicación enviada",Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Error al hacer la publicacion", Snackbar.LENGTH_INDEFINITE).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                finish();
                Toast.makeText(getApplicationContext(), "Error en el servidor "+error,Toast.LENGTH_LONG).show();
               // Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "El servidor no responde, reintente nuevamente", Snackbar.LENGTH_INDEFINITE).show();
            }
        });
        array.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(array);
    }

    //Categorias
    private void cargarCategorias(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL_TRAER_CATEGORIAS,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject j = null;
                try {
                    j = new JSONObject(response);
                    resultCategorias = j.getJSONArray("categorias");
                    obtenerCategorias(resultCategorias);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Sin Respuesta "+error, Toast.LENGTH_LONG).show();
                cargarCategorias();
            }
        });
        requestQueue.add(stringRequest);
    }

    public void obtenerCategorias(JSONArray j){
        ArrayList<Categorias> categoriasList = new ArrayList<>();
        for(int i=0;i<j.length();i++){
            try{
                JSONObject json = j.getJSONObject(i);
                String id_categoria;
                String nombre_categoria;
                id_categoria = json.getString("id_categoria");
                nombre_categoria = json.getString("nombre_categoria");
                categoriasList.add(new Categorias(id_categoria,nombre_categoria));

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        ArrayAdapter<Categorias> adapter = new ArrayAdapter<Categorias>(this, android.R.layout.simple_spinner_dropdown_item, categoriasList);
        spinnerCategorias.setAdapter(adapter);
    }

   /* private String obtenerDireccion(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Toast.makeText(getApplicationContext(),strReturnedAddress.toString(),Toast.LENGTH_LONG).show();
                Log.w("My Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }*/

    //Todo lo que tiene que ver con Google Location ...
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Obtenemos la última ubicación al ser la primera vez
        processLastLocation();
        // Iniciamos las actualizaciones de ubicación
        startLocationUpdates();
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi
                .removeLocationUpdates(mGoogleApiClient, this);
    }

    private void processLastLocation() {
        getLastLocation();
        if (mLastLocation != null) {
            updateLocationUI();
        }
    }

    private void getLastLocation() {
        if (isLocationPermissionGranted()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } else {
            manageDeniedPermission();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Conexión suspendida");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(
                this,
                "Error de conexión con el código:" + connectionResult.getErrorCode(),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Detección de actividad iniciada");

        } else {
            Log.e(TAG, "Error al iniciar/remover la detección de actividad: "
                    + status.getStatusMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, String.format("Nueva ubicación: (%s, %s)",
                location.getLatitude(), location.getLongitude()));
        mLastLocation = location;
        updateLocationUI();
    }

    private synchronized void builGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .enableAutoManage(this, this)
                .build();
    }
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest)
                .setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest()
                .setInterval(1000)
                .setFastestInterval(1000/2)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //Actualiza el UI con la unicacion
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(LOCATION_KEY)) {
                mLastLocation = savedInstanceState.getParcelable(LOCATION_KEY);
                updateLocationUI();
            }
        }
    }
    private void updateLocationUI() {
        // mLatitude.setText(String.valueOf(mLastLocation.getLatitude()));
        //mLongitude.setText(String.valueOf(mLastLocation.getLongitude()));
        direccionPub = obtenerDireccion(mLastLocation.getLatitude(),mLastLocation.getLongitude());

        //edt_direccion.setText(direccionPub);
        latitudPub = String.valueOf(mLastLocation.getLatitude());
        longitudPub = String.valueOf(mLastLocation.getLongitude());
        //txt_longitud.setText(" Longitud "+longitudGlob);
        //txt_latitud.setText("Latitud "+latitudGlob);

        //  edt_ubicacion_actual.setText("Aqui la ubicacion con geocoder");
    }

    private String obtenerDireccion(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                String pais = returnedAddress.getCountryName();

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString()+pais;
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void verificarAjustes() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient, mLocationSettingsRequest
                );
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "Los ajustes de ubicación satisfacen la configuración.");
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            Log.d(TAG, "Los ajustes de ubicación no satisfacen la configuración. " +
                                    "Se mostrará un diálogo de ayuda.");
                            status.startResolutionForResult(
                                    Agregar_publicacion.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.d(TAG, "El Intent del diálogo no funcionó.");
                            // Sin operaciones
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.d(TAG, "Los ajustes de ubicación no son apropiados.");
                        break;

                }
            }
        });
    }

    private void startLocationUpdates() {
        if (isLocationPermissionGranted()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else {
            manageDeniedPermission();
        }
    }

    private void manageDeniedPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Aquí muestras confirmación explicativa al usuario
            // por si rechazó los permisos anteriormente
        } else {
            ActivityCompat.requestPermissions(
                    this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    private boolean isLocationPermissionGranted() {
        int permission = ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        return permission == PackageManager.PERMISSION_GRANTED;
    }
}