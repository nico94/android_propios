package com.aplicacion.findgo.findgo.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Nicolas on 10-11-2016.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final String TAG = DbHelper.class.getSimpleName();
    public static final String DB_NAME = "Findgo.db";
    public static final int DB_VERSION = 1;

    public static final String TABLA_USUARIO = "usuarios";
    public static final String ID = "id";

    public static final String COLUMNA_USUARIO = "usuario";
    public static final String COLUMNA_PASSWORD = "password";
    public static final String COLUMNA_NOMBRES = "nombres";
    public static final String COLUMNA_APELLIDOS = "apellidos";
    public static final String COLUMNA_TELEFONO = "telefono";
    public static final String COLUMNA_DIRECCION = "direccion";
    public static final String COLUMNA_EMAIL = "email";
    public static final String COLUMNA_ID_VALORACION = "id_valoracion";
    public static final String COLUMNA_ID_ROLES = "id_roles";

    //Crear la tabla de usuarios
    public static final String CREATE_TABLA_USUARIO = "CREATE TABLE "+ TABLA_USUARIO +
            "("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMNA_USUARIO + " TEXT,"
            + COLUMNA_PASSWORD   + " TEXT,"
            + COLUMNA_NOMBRES    + " TEXT,"
            + COLUMNA_APELLIDOS + " TEXT,"
            + COLUMNA_TELEFONO + " TEXT,"
            + COLUMNA_DIRECCION + " TEXT,"
            + COLUMNA_EMAIL + " TEXT,"
            + COLUMNA_ID_VALORACION + " TEXT,"
            + COLUMNA_ID_ROLES + " TEXT);";


    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLA_USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_USUARIO);
        onCreate(db);
    }

    public void agregaUsuario(String usuario, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMNA_USUARIO, usuario);
        values.put(COLUMNA_PASSWORD, password);

        long id = db.insert(TABLA_USUARIO, null, values);
        Log.d(TAG, "usuario agregado " + usuario + " - " + password);

    }

    public void agregaDatosUsuario(String usuario, String password, String nombres, String apellidos,
                                   String telefono, String direccion, String email, String id_valoracion, String id_roles ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_USUARIO, usuario);
        values.put(COLUMNA_PASSWORD, password);
        values.put(COLUMNA_NOMBRES, nombres);
        values.put(COLUMNA_APELLIDOS, apellidos);
        values.put(COLUMNA_TELEFONO, telefono);
        values.put(COLUMNA_DIRECCION, direccion);
        values.put(COLUMNA_EMAIL, email);
        values.put(COLUMNA_ID_VALORACION, id_valoracion);
        values.put(COLUMNA_ID_ROLES, id_roles);
        db.insert(TABLA_USUARIO,null, values);

        long id = db.insert(TABLA_USUARIO, null, values);
        Log.d(TAG, "Datos de usuario agregados: " +usuario+" - "+password+" - "+nombres+" - "+apellidos+
                " - "+telefono+" - "+direccion+" - "+email+" - "+ id_valoracion+" - "+id_roles);

    }

    public void eliminarUsuario(){
        String deleteQuery = "delete * from "+TABLA_USUARIO;
        SQLiteDatabase db = this.getWritableDatabase();
        long elimina = db.delete(TABLA_USUARIO,null,null);
       // Cursor cursor = db.rawQuery(deleteQuery,null);
        Log.d(TAG, "Se elimina la tabla "+elimina);
    }

    public String[] obtenerDatosUsuario(){
        String selectQuery = "select * from "+TABLA_USUARIO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        String usuario="";
        String password="";
        String nombres="";
        String apellidos="";
        String telefono="";
        String direccion="";
        String email="";
        String id_valoracion="";
        String id_roles="";

        if(cursor.moveToFirst()){
            do{
                usuario = cursor.getString(cursor.getColumnIndex(COLUMNA_USUARIO));
                password = cursor.getString(cursor.getColumnIndex(COLUMNA_PASSWORD));
                nombres = cursor.getString(cursor.getColumnIndex(COLUMNA_NOMBRES));
                apellidos = cursor.getString(cursor.getColumnIndex(COLUMNA_APELLIDOS));
                telefono = cursor.getString(cursor.getColumnIndex(COLUMNA_TELEFONO));
                direccion = cursor.getString(cursor.getColumnIndex(COLUMNA_DIRECCION));
                email = cursor.getString(cursor.getColumnIndex(COLUMNA_EMAIL));
                id_valoracion = cursor.getString(cursor.getColumnIndex(COLUMNA_ID_VALORACION));
                id_roles = cursor.getString(cursor.getColumnIndex(COLUMNA_ID_ROLES));
            }while (cursor.moveToNext());
        }
        Log.d(TAG, "usuario: "+usuario+ " pass: "+password+" nombres: "+nombres+" apellido: "+apellidos +
                " telefono: "+telefono+" direccion: " +direccion + " email: "+email+ " valoracion "+id_valoracion +" rol: "+id_roles);
        String[] datos = {usuario,password,nombres,apellidos,telefono,direccion,email};

        db.close();
        return datos;
    }


    public boolean obtenerUsuarioConLogin(String usuario, String password) {
        String selectQuery = "select * from " + TABLA_USUARIO + " where " +
                COLUMNA_USUARIO + " = " + "'" + usuario + "'" + " and " + COLUMNA_PASSWORD + " = " + "'" + password + "'";

        Log.d(TAG, "consulta bd " + selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }
}