package com.aplicacion.findgo.findgo.WebServices;

/**
 * Created by Nicolas on 08-11-2016.
 */

public class Constant {

    public static String URL = "http://www.findgo.cl/";
    public static String URL1 = "http://192.168.1.92/FindGoWS/";

    public static String URL_ENVIAR_TOKEN = URL+"application/firebase_server/firebase_server.php";
    public static String URL_ENVIAR_NOTIFICACION = URL+"application/firebase_server/enviar_notificacion.php";


    public static String URL_IMAGENES = "http://www.findGo.cl/public/img/";
    public static String URL_IMAGENES1 = "http://192.168.1.92/FindGo/public/img/";

    public static String URL_TRAER_PUBLICACIONES = URL + "ws_publicaciones";
    public static String URL_TRAER_ALERTAS = URL + "ws_publicaciones/listar_alertas/";
    public static String URL_DATOS_PUBLICACION = URL + "ws_publicaciones/datos_publicacion/";
    public static String URL_TRAER_CATEGORIAS = URL + "ws_publicaciones/listar_categorias/";
    public static String URL_TRAER_COMENTARIOS = URL + "ws_publicaciones/comentarios_publicacion/";
    public static String URL_MOSTRAR_TIPOS_ALERTAS = URL + "ws_publicaciones/tipos_alertas/";

    public static String URL_LOGIN = URL + "ws_usuarios/login/";
    public static String URL_DATOS_USUARIO = URL + "ws_usuarios/datos_usuario/";
    public static String URL_VALIDA_USUARIO = URL + "ws_usuarios/valida_usuario/";
    public static String URL_REGISTRO = URL + "ws_usuarios/registro/";
    public static String URL_ACTUALIZAR_FOTO_PERFIL = URL + "ws_usuarios/actualizar_foto_perfil/";
    public static String URL_ACTUALIZAR_PERFIL = URL + "ws_usuarios/actualizar_perfil/";


    public static String URL_AGREGAR_PUBLICACION = URL+"ws_publicaciones/agregar_publicacion/";
    public static String URL_AGREGAR_COMENTARIO = URL+"ws_publicaciones/agregar_comentario/";
    public static String URL_ENVIAR_ALERTA = URL + "ws_publicaciones/agregar_alerta/";
    public static String URL_SUBIDA = "http://www.findgo.cl/subir_imagen.php";

}
