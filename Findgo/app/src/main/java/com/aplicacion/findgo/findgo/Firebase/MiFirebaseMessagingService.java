package com.aplicacion.findgo.findgo.Firebase;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aplicacion.findgo.findgo.Detalle_publicacion;
import com.aplicacion.findgo.findgo.MainActivity;
import com.aplicacion.findgo.findgo.R;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Nicolas on 09-12-2016.
 */

public class MiFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{

    private static final String TAG = "Notificacion";
    public String usuarioNot = null;
    public String tipoNot = null;
    public String postNot = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String from = remoteMessage.getFrom();
        Log.d(TAG,"Mensaje de: "+from);

        if (remoteMessage.getNotification() !=null){
           mostrarNotificacion(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
        }
        if (remoteMessage.getData().size() > 0){

           // String mensaje = remoteMessage.getData().get("message");
           // Bundle bundle = new Bundle();
            //bundle.putString("message", mensaje);

            usuarioNot = remoteMessage.getData().get("usuario");
            tipoNot = remoteMessage.getData().get("tipo");
            postNot = remoteMessage.getData().get("post");

            Log.d(TAG,"usuario: "+usuarioNot+ " tipo: "+tipoNot+" post: "+postNot);

        }
    }

    private void mostrarNotificacion(String title, String body) {
        Intent intent = new Intent(this, Detalle_publicacion.class);
        intent.putExtra("id_post",postNot);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_ONE_SHOT);

        Uri notificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logopng)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(notificacion)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

    }
}
