package com.aplicacion.findgo.findgo.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aplicacion.findgo.findgo.Adaptadores.Adaptador_Publicaciones;
import com.aplicacion.findgo.findgo.Modelos.Publicaciones;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_IMAGENES;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_TRAER_PUBLICACIONES;

/**
 * Created by Nicolas on 08-09-2016.
 */
public class Pagina1 extends Fragment {
    //WS
    public VolleyS volley;
    public RequestQueue requestQueue;
    //Componentes
    ImageView imageView;
    //Lista de publicaciones
    public List<Publicaciones> listPublicaciones = new ArrayList<>();
    public Publicaciones publi;
    JsonObjectRequest array;
    //Inicializar variables
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    Adaptador_Publicaciones adaptador_publicaciones = new Adaptador_Publicaciones(listPublicaciones,getContext());
    //Componentes
    SwipeRefreshLayout swipeRefreshLayout;
    Thread hiloMostrarPublicaciones;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //"Inflamos" el archivo XML correspondiente a esta sección.
        View rootView = inflater.inflate(R.layout.fragment_pagina1,container,false);
        //Inicializar la lista de publicaciones vacia
        listPublicaciones = new ArrayList<>();
        //RecyclerView
        recyclerView = (RecyclerView) rootView.findViewById(R.id.reciclador);
        recyclerView.setHasFixedSize(true);

        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adaptador_publicaciones);
        //Peticiones al ws
        volley = VolleyS.getInstance(getActivity().getApplication());
        requestQueue = volley.getmRequestQueue();
        imageView = (ImageView) rootView.findViewById(R.id.imagen_tarjeta);
        //Hilo para cargar las publicaciones mas pesadas

        hiloMostrarPublicaciones = new Thread(){
            @Override
            public void run(){
                try{
                    //Hacer el metodo
                    mostrarPublicaciones();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        hiloMostrarPublicaciones.start();

        //Swipe Refresh
        swipeRefreshLayout =(SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Lompia la lista
                listPublicaciones = new ArrayList<>();

                mostrarPublicaciones();

                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //Retorno la vista root del fragment
        return rootView;
    }

    private synchronized Boolean pararHilos(Thread hilo){
        if(hilo != null){
            hilo = null;
            return true;
        }
        return false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);
    }

    @Override
    public void onPause(){
        hiloMostrarPublicaciones.interrupt();
        super.onPause();
    }
    @Override
    public void onResume(){
        super.onResume();
    }

    //Funcion para mostrar las publicaciones.
    public void mostrarPublicaciones(){
        //Progress Bar
        //final ProgressDialog cargando = new ProgressDialog(getActivity());
        //cargando.setMessage("Cargando, espere...");
        //cargando.show();
        //Guardar la respuesta en "request" que viene de la url declarada en Constans
        array = new JsonObjectRequest(Request.Method.GET, URL_TRAER_PUBLICACIONES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //cargando.dismiss();
                try {
                    JSONArray json = response.getJSONArray("publicaciones");
                   parsearDatos(json);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError volleyError){
                //Servidor no responde, mostrar Snackbar infinito con boton reintentar presionar ejecuta metodo mostrarPublicaciones()
                //cargando.cancel();
                    Snackbar.make(getView(), "No hay respuesta del servidor", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Reintentar", new View.OnClickListener(){
                        @Override
                        public void onClick(View view){
                            mostrarPublicaciones();
                        }
                    }).show();
                }
        });
        array.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(array);
    }

    //Metodo parseo de datos del Json
    private void parsearDatos(JSONArray response){
        for (int i = 0; i<response.length();i++){
            Publicaciones publi = new Publicaciones();
            JSONObject json = null;
            try{
                json = response.getJSONObject(i);
                publi.setId_post(json.optString("id_post"));
                publi.setId_categoria(json.optString("nombre_categoria"));
                publi.setPost(json.optString("post"));
                publi.setContenido(json.optString("contenido"));
                publi.setImagen(URL_IMAGENES+json.optString("imagen"));
                publi.setFecha(json.optString("fecha"));
                publi.setHora(json.optString("hora"));
                publi.setUsuario(json.optString("usuario"));
                publi.setId_ubicacion(json.optString("ubicacion"));
                publi.setLatitud(json.optString("latitud"));
                publi.setLongitud(json.optString("longitud"));

               // publi.setImagen_user(URL_IMAGENES+json.optString("imagen_user"));
            }catch (JSONException e){
                e.printStackTrace();
            }
            //Agregar la petiicon al ws
            listPublicaciones.add(publi);
        }
        //Inicializar el adaptador
        adapter = new Adaptador_Publicaciones(listPublicaciones, getContext());
        //Agregar el adaptador al RecyclerViw
        recyclerView.setAdapter(adapter);
    }
}