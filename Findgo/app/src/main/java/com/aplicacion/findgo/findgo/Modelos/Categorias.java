package com.aplicacion.findgo.findgo.Modelos;

/**
 * Created by Nicolas on 24-11-2016.
 */

public class Categorias {

    String id_categoria;
    String nombre_categoria;

    public Categorias(String id_categoria, String nombre_categoria) {
        this.id_categoria = id_categoria;
        this.nombre_categoria = nombre_categoria;
    }

    public String getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(String id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre_categoria() {
        return nombre_categoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return nombre_categoria;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Categorias){
            Categorias c = (Categorias) obj;
            if(c.getNombre_categoria().equals(nombre_categoria) && c.getId_categoria()==id_categoria )return true;
        }
        return false;
    }

}

