package com.aplicacion.findgo.findgo.Firebase;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nicolas on 11-12-2016.
 */

public class EnviarNotificacion {
    String titulo = null;
    String mensaje = null;
    String tipo = null;
    String usuario = null;
    String post = null;
    Context context;

    VolleyS volley;
    RequestQueue requestQueue;

    public EnviarNotificacion(Context c, String titulo, String mensaje, String tipo, String usuario, String post) {
        context = c;
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.tipo = tipo;
        this.usuario = usuario;
        this.post = post;

        //Inicializar el volley
        volley = VolleyS.getInstance(context);
        requestQueue = volley.getmRequestQueue();
    }

    public void enviarAlServer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_ENVIAR_NOTIFICACION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(context,"Notificacion enviada "+usuario,Toast.LENGTH_LONG).show();
                       // Toast.makeText(context,"Respuesta Firebase: "+ response.toString(),Toast.LENGTH_LONG).show();
                        System.out.println(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Error al notificar al usuario "+error,Toast.LENGTH_LONG);
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("title", titulo);
                params.put("message", mensaje);
                params.put("tipo", tipo);
                params.put("usuario", usuario);
                params.put("post", post);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
