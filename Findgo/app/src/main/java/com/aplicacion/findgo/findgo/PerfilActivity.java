package com.aplicacion.findgo.findgo;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_ACTUALIZAR_PERFIL;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_IMAGENES;
import static com.aplicacion.findgo.findgo.WebServices.Constant.URL_SUBIDA;

public class PerfilActivity extends AppCompatActivity {

    DbHelper db;

    public VolleyS volley;
    public RequestQueue requestQueue;
    JsonObjectRequest array, array2, array3;
    ImageLoader imageLoader;

    EditText edt_nombres, edt_apellidos, edt_telefono, edt_direccion, edt_email, edt_pass1, edt_pass2;
    TextInputLayout input_email;
    TextView titulo;
    CircleImageView foto_perfil;
    Button boton_guardar;

    String nuevaFotoPerfil = null;
    String usuarioGlob = null;

    private Bitmap bitmap;
    private Uri filePath;

    public static  final int SELECCION_IMAGEN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        titulo = (TextView) findViewById(R.id.perfil_titulo);
        edt_nombres = (EditText) findViewById(R.id.perfil_nombres);
        edt_apellidos = (EditText) findViewById(R.id.perfil_apellidos);
        edt_telefono = (EditText) findViewById(R.id.perfil_telefono);
        edt_direccion = (EditText) findViewById(R.id.perfil_direccion);
        edt_email =(EditText) findViewById(R.id.perfil_email);
        edt_pass1 = (EditText) findViewById(R.id.perfil_pass1);
        edt_pass2 = (EditText) findViewById(R.id.perfil_pass2);
        input_email = (TextInputLayout) findViewById(R.id.perfil_input_email);
        foto_perfil = (CircleImageView) findViewById(R.id.perfil_imagen);
        boton_guardar = (Button) findViewById(R.id.boton_guardar);
        edt_email.addTextChangedListener(new PerfilActivity.MiVisor(edt_email));

        db = new DbHelper(this);
        String resp[] = db.obtenerDatosUsuario();
        usuarioGlob = resp[0];

        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();

        traerDatos(usuarioGlob);
        clickImagen();
        botonGuardar();

    }

    public void botonGuardar(){
        boton_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Aqui rescatar los datos
                String nombreR = edt_nombres.getText().toString();
                String apellidoR = edt_apellidos.getText().toString();
                String telefonoR = edt_telefono.getText().toString();
                String direccionR = edt_direccion.getText().toString();
                String emailR = edt_email.getText().toString();
                String passwordR = edt_pass1.getText().toString();
                String passwordR2 = edt_pass2.getText().toString();

                //Valida los EditText
                if (edt_nombres.getText().length() == 0) {
                    Snackbar.make(v, "Nombre requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_apellidos.getText().length() == 0) {
                    Snackbar.make(v, "Apellido requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_pass1.getText().length() == 0) {
                    Snackbar.make(v, "Contraseña requerida", Snackbar.LENGTH_LONG).show();
                } else if (edt_direccion.getText().length() == 0) {
                    Snackbar.make(v, "Dirección requerida", Snackbar.LENGTH_LONG).show();
                } else if (edt_telefono.getText().length() == 0) {
                    Snackbar.make(v, "Teléfono requerido", Snackbar.LENGTH_LONG).show();
                } else if (edt_email.getText().length() == 0 || !validarCorreo()) {
                    Snackbar.make(v, "Email requerido", Snackbar.LENGTH_LONG).show();
                } else {
                    if (passwordR.equalsIgnoreCase(passwordR2)) {
                        //Codificar los datos en formato URL
                        String usuarioURL = URLEncoder.encode(usuarioGlob).replace("+","%20");
                        String passwordURL = URLEncoder.encode(passwordR).replace("+", "%20");
                        String nombreURL = URLEncoder.encode(nombreR).replace("+", "%20");
                        String apellidoURL = URLEncoder.encode(apellidoR).replace("+", "%20");
                        String telefonoURL = URLEncoder.encode(telefonoR).replace("+", "%20");
                        String direccionURL = URLEncoder.encode(direccionR).replace("+", "%20");
                        String emailURL = URLEncoder.encode(emailR).replace("+", "%20");

                        //Aqui es todo valido
                        enviarDatos(usuarioURL,passwordR2,nombreURL,apellidoURL,telefonoURL,direccionURL,emailURL);

                    }else {
                        Snackbar.make(v, "Las contraseñas no coinciden", Snackbar.LENGTH_LONG).show();
                        Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                        edt_pass1.startAnimation(shake);
                        edt_pass2.startAnimation(shake);
                    }
                }
            }
        });
    }

    private class MiVisor implements TextWatcher {
        private View view;

        private MiVisor(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edt_correo:
                    validarCorreo();
                    break;
            }
        }
    }

    private boolean validarCorreo() {
        String email = edt_email.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            input_email.setError("ingrese un email válido");
            //requestFocus(input_email);
            return false;
        } else {
            input_email.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void clickImagen(){
        foto_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Seleccione una imagen"),SELECCION_IMAGEN);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK){
                filePath = data.getData();
                    if(null != filePath) {
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
                            String path = getPath(filePath);
                            //System.out.println(path);
                            String nombreImagen = path.substring(path.lastIndexOf("/")+1);
                            nuevaFotoPerfil = nombreImagen;
                            foto_perfil.setImageBitmap(bitmap);

                            actualizaFoto(usuarioGlob,nuevaFotoPerfil);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return;
        }
    }

    public void uploadMultiPart() {
        String name = nuevaFotoPerfil;
        String path = getPath(filePath);

        try {
            String uploadId = UUID.randomUUID().toString();
            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, URL_SUBIDA)
                    .addFileToUpload(path, "image") //Adding file
                    .addParameter("name", name) //Adding text parameter to the request
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(3)
                    .startUpload(); //Starting the upload

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    public void traerDatos(final String usuario){
        array = new JsonObjectRequest(Request.Method.GET, Constant.URL_DATOS_USUARIO+"/"+usuario, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray respuesta = response.optJSONArray("datos");
                    String usuarioResponse = "";
                    String passwordResponse = "";
                    String nombresResponse = "";
                    String apellidosResponse = "";
                    String telefonoResponse = "";
                    String direccionResponse = "";
                    String emailResponse = "";
                    String imagenPerfilResponse = "";
                    String id_valoracionResponse = "";
                    String id_rolesResponse = "";

                    for (int i = 0; i < respuesta.length(); i++) {
                        JSONObject jsonChildNode = respuesta.getJSONObject(i);
                        usuarioResponse = jsonChildNode.optString("usuario");
                        passwordResponse = jsonChildNode.optString("password");
                        nombresResponse = jsonChildNode.optString("nombres");
                        apellidosResponse = jsonChildNode.optString("apellidos");
                        telefonoResponse = jsonChildNode.optString("telefono");
                        direccionResponse = jsonChildNode.optString("direccion");
                        emailResponse = jsonChildNode.optString("email");
                        imagenPerfilResponse = jsonChildNode.optString("imagen_user");
                        id_valoracionResponse = jsonChildNode.optString("id_valoracion");
                        id_rolesResponse = jsonChildNode.optString("id_roles");
                    }
                    byte[] data = Base64.decode(passwordResponse, Base64.DEFAULT);
                    String passDecod = new String(data, "UTF-8");

                    String imagenUrl = URL_IMAGENES+imagenPerfilResponse;
                    Picasso.with(getApplicationContext()).load(imagenUrl)
                            .placeholder(R.drawable.usuario_card)
                            .error(R.drawable.usuario_card)
                            .fit()
                            .centerCrop()
                            .into(foto_perfil);

                    titulo.setText(usuarioResponse);
                    edt_nombres.setText(nombresResponse);
                    edt_apellidos.setText(apellidosResponse);
                    edt_telefono.setText(telefonoResponse);
                    edt_direccion.setText(direccionResponse);
                    edt_email.setText(emailResponse);
                    edt_pass1.setText(passDecod);
                    edt_pass2.setText(passDecod);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                finish();
            }
        });
        requestQueue.add(array);
    }

    public void enviarDatos(String usuario, String password, String nombres, String apellidos, String telefono, String direccion, String email){

        String cadena = usuario+"/"+password+"/"+nombres+"/"+apellidos+"/"+telefono+"/"+direccion+"/"+email;

        array2 = new JsonObjectRequest(Request.Method.POST, URL_ACTUALIZAR_PERFIL+cadena, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Si la respuesta es positiva
                String verificacion = response.optString("actualiza");
                if(verificacion.equals("true")){
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Perfil actualizado con exito", Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Error al actualizar el perfil", Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Si el servidor no responde
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(array2);
    }

    public void actualizaFoto(String usuario, String foto){
        String usuarioURL = URLEncoder.encode(usuario).replace("+","%20");
        String fotoURL = URLEncoder.encode(foto).replace("+","%20");

        String cadena = usuarioURL+"/"+fotoURL;
        array3 = new JsonObjectRequest(Request.Method.POST, Constant.URL_ACTUALIZAR_FOTO_PERFIL + cadena,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Manejar la respuesta
                String verificacion = response.optString("actualiza");
                if(verificacion.equals("true")){
                    uploadMultiPart();
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Foto de perfil actualizada", Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Error al actualizar foto de perfil", Snackbar.LENGTH_INDEFINITE).show();
                }
                //Toast.makeText(getApplicationContext(),respuesta.toString(),Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(array3);
    }
}
