package com.aplicacion.findgo.findgo.WebServices;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Nicolas on 08-09-2016.
 */

//Clase para obtener el contexto de la aplicacion
public class MyApplication extends Application {

    public static  final String TAG = MyApplication.class.getSimpleName();
    private RequestQueue requestQueue;
    private static MyApplication mInstance;

//    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        //this.setAppContext(getApplicationContext());
    }

    public static synchronized MyApplication getInstance(){
        return mInstance;
    }
    public RequestQueue getRequestQueue(){
        if(requestQueue == null){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueque(Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
    public <T> void addToRequestQueque(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequest(Object tag){
        if(requestQueue != null){
            requestQueue.cancelAll(tag);
        }
    }

        // public static Context getAppContext() {
     //   return mAppContext;
    //}
    //public void setAppContext(Context mAppContext) {
      //  this.mAppContext = mAppContext;
    //}
}
