package com.aplicacion.findgo.findgo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.aplicacion.findgo.findgo.Adaptadores.Adaptador_ViewPagerPrincipal;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.BD.Sesion;
import com.aplicacion.findgo.findgo.Login.login;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    //Llamar a la clase
    private Adaptador_ViewPagerPrincipal adaptador_ViewPagerPrincipal;
    private ViewPager viewPager;
    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    Animation animation;
    //Objeto de sesion
    Sesion sesion;
    //DB SQLite
    DbHelper db;
    //Objeto Json guardado en un array
    public VolleyS volley;
    public RequestQueue requestQueue;
    JsonObjectRequest array;

    String usuarioM = "";
    String passwordM = "";
    String nombresM = "";
    String apellidosM = "";
    String telefonoM = "";
    String direccionM = "";
    String emailM = "";
    Thread hiloVerificaLogin;
    private static final int SOLICITUD_PERMISOS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        solicitarPermisos();

        db = new DbHelper(this);
        String resp[] = db.obtenerDatosUsuario();

        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();

        // Iniciamos la barra de herramientas.
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarPrincipal);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.AppbarPrincipal);
        collapsingToolbarLayout.setTitleEnabled(false);

        //Fabs
        fab1 = (FloatingActionButton) findViewById(R.id.fab_publicacion);
        fab2 = (FloatingActionButton) findViewById(R.id.fab_advertencia);
        // Iniciamos la barra de tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.TabLayoutPrincipal);
        //Verificar la Sesion
        sesion = new Sesion(this);
        if (!sesion.loggein()) {
            finish();
        }

        usuarioM = resp[0];
        passwordM = resp[1];
        nombresM = resp[2];
        apellidosM = resp[3];
        telefonoM = resp[4];
        direccionM = resp[5];
        emailM = resp[6];

        verificarLogin(usuarioM, passwordM, nombresM, apellidosM, telefonoM, direccionM, emailM);
        enviarToken();

        // Añadimos las 3 tabs de las secciones.
        // Le damos modo "fixed" para que todas las tabs tengan el mismo tamaño. También le asignamos una gravedad centrada.
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        //tabLayout.addTab(tabLayout.newTab());
        animation = (Animation) AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_to_right);
        // Iniciamos el viewPager.
        viewPager = (ViewPager) findViewById(R.id.ViewPagerPrincipal);
        // Creamos el adaptador, al cual le pasamos por parámtro el gestor de Fragmentos y muy importante, el nº de tabs o secciones que hemos creado.
        adaptador_ViewPagerPrincipal = new Adaptador_ViewPagerPrincipal(getSupportFragmentManager(), tabLayout.getTabCount());
        // Y los vinculamos.
        viewPager.setAdapter(adaptador_ViewPagerPrincipal);
        // Y por último, vinculamos el viewpager con el control de tabs para sincronizar ambos.
        showRightFab(viewPager.getCurrentItem());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                showRightFab(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.setupWithViewPager(viewPager);
        //Funciones toolbar
        setSupportActionBar(toolbar);
        toolbar.setAnimation(animation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Bienvenido");
        getSupportActionBar().setSubtitle(nombresM + " " + apellidosM);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    public void solicitarPermisos() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            // Toast.makeText(this, "1 Permiso Concedido", Toast.LENGTH_SHORT).show();

        } else {
            explicarUsoPermiso();
            solicitarPermiso();
        }
    }

    private void solicitarPermiso() {
        //Pedimos el permiso o los permisos con un cuadro de dialogo del sistema
        ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                SOLICITUD_PERMISOS);
    }

    private void explicarUsoPermiso() {
        //Este IF es necesario para saber si el usuario ha marcado o no la casilla [] No volver a preguntar
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Toast.makeText(this, "2'.1 Explicamos razonadamente porque necesitamos el permiso", Toast.LENGTH_SHORT).show();
            //Explicarle al usuario porque necesitas el permiso (Opcional)
            //alertDialog();
        }
    }

    private boolean isPermissionGranted() {
        int permission = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /**
         * Si tubieramos diferentes permisos solicitando permisos de la aplicacion, aqui habria varios IF
         */
        if (requestCode == SOLICITUD_PERMISOS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Realizamos la accion
                //Toast.makeText(this, "Permisos consedidos", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(this, "3.2 Permiso No Concedido", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void alertDialog() {
        // 1. Instancia de AlertDialog.Builder con este constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // 2. Encadenar varios métodos setter para ajustar las características del diálogo
        builder.setMessage("Sin permisos. No podremos saber tu ubicación ni leer tus imágenes para publicarlas.");
        builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.show();
    }

    public void enviarToken() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_ENVIAR_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response.toString());
                        //Toast.makeText(getApplicationContext(),"Se envio: "+token,Toast.LENGTH_LONG).show();
                        //Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error al enviar token", Toast.LENGTH_LONG).show();
            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fcm_token", token);
                params.put("usuario", usuarioM);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onResume() {
        verificarLogin(usuarioM, passwordM, nombresM, apellidosM, telefonoM, direccionM, emailM);
        // getSupportActionBar().setSubtitle(nombresM+" "+apellidosM);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: //hago un case por si en un futuro agrego mas opciones
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setMessage("¿Está seguro que quiere salir de FindGo?");
                builder2.setTitle("Salir");
                builder2.setIcon(R.drawable.exit);
                builder2.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                builder2.setNegativeButton("Quedarme", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder2.create();
                dialog.show();
                return true;
            case R.id.perfil:
                //Aqui lanzar la vista para el perfil
                Intent intent = new Intent(MainActivity.this, PerfilActivity.class);
                startActivity(intent);
                return true;
            case R.id.cerrar_sesion:
                //Aqui lanzar una advertencia Yes/no
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("¿Está seguro que quiere cerrar sesión?");
                builder.setTitle("Cerrar sesión");
                builder.setIcon(R.drawable.puerta);
                builder.setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Logout
                        //sesion.setLoggedin(false);
                        Intent intent = new Intent(MainActivity.this, login.class);
                        //intent.putExtra("usuario", usuario);
                        sesion.setLoggedin(false);
                        db.eliminarUsuario();
                        finish();
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Quedarme", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialogSesion = builder.create();
                dialogSesion.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showRightFab(int tab) {
        switch (tab) {
            case 0:
                //fab2.setVisibility(View.GONE);
                //fab1.setVisibility(View.VISIBLE);
                fab2.hide(new FloatingActionButton.OnVisibilityChangedListener() {
                    @Override
                    public void onHidden(FloatingActionButton fab) {
                        fab1.show();
                    }
                });
                fab1.setOnClickListener(new FloatingActionButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(getApplicationContext(),"Boton publicaciones",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MainActivity.this, Agregar_publicacion.class);
                        startActivity(intent);
                    }
                });
                break;
            case 1:
                //fab1.setVisibility(View.GONE);
                //fab2.setVisibility(View.VISIBLE);
                fab1.hide(new FloatingActionButton.OnVisibilityChangedListener() {
                    @Override
                    public void onHidden(FloatingActionButton fab) {
                        fab2.show();
                    }
                });
                fab2.setOnClickListener(new FloatingActionButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent agregarAlerta = new Intent(MainActivity.this, Agregar_alerta.class);
                        startActivity(agregarAlerta);
                        //Toast.makeText(getApplicationContext(),"Boton alertas",Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case 2:
                fab1.hide();
                fab2.hide();
                break;
            default:
                fab1.show();
                fab2.show();
                break;
        }
    }

    public void verificarLogin(String usuario, final String password1, final String nombre, final String apellido, final String telefono, final String direccion, final String email) {

        array = new JsonObjectRequest(Request.Method.GET, Constant.URL_DATOS_USUARIO + usuario, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Manejar la respuesta
                try {
                    JSONArray respuesta = response.optJSONArray("datos");
                    String usuarioResponse = "";
                    String passwordResponse = "";
                    String nombresResponse = "";
                    String apellidosResponse = "";
                    String telefonoResponse = "";
                    String direccionResponse = "";
                    String emailResponse = "";
                    String id_valoracionResponse = "";
                    String id_rolesResponse = "";

                    //Recorrer el arreglo Json, sacarle los strings y guardarlo en variables
                    for (int i = 0; i < respuesta.length(); i++) {
                        JSONObject jsonChildNode = respuesta.getJSONObject(i);
                        usuarioResponse = jsonChildNode.optString("usuario");
                        passwordResponse = jsonChildNode.optString("password");
                        nombresResponse = jsonChildNode.optString("nombres");
                        apellidosResponse = jsonChildNode.optString("apellidos");
                        telefonoResponse = jsonChildNode.optString("telefono");
                        direccionResponse = jsonChildNode.optString("direccion");
                        emailResponse = jsonChildNode.optString("email");
                        id_valoracionResponse = jsonChildNode.optString("id_valoracion");
                        id_rolesResponse = jsonChildNode.optString("id_roles");
                    }
                    byte[] data = Base64.decode(passwordResponse, Base64.DEFAULT);
                    String passDecod = new String(data, "UTF-8");

                    //Toast.makeText(getApplicationContext(),"passWS: "+passwordResponse+" | passBD: "+password1,Toast.LENGTH_LONG).show();
                    if (!passDecod.equals(password1)) {
                        //Usuario cambio la contraseña abrir login de nuevo
                        Toast.makeText(getApplicationContext(), "Cambió su contraseña, inicie sesión nuevamente", Toast.LENGTH_LONG).show();
                        sesion.setLoggedin(false);
                        Intent intent = new Intent(MainActivity.this, login.class);
                        startActivity(intent);
                        finish();
                    }
                    if (!nombre.equals(nombresResponse) || !apellido.equals(apellidosResponse) || !telefono.equals(telefonoResponse) || !direccion.equals(direccionResponse) || !email.equals(emailResponse)) {
                        //Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Datos de usuario actualizados", Snackbar.LENGTH_LONG).show();
                        db.eliminarUsuario();
                        db.agregaDatosUsuario(usuarioResponse, passDecod, nombresResponse,
                                apellidosResponse, telefonoResponse, direccionResponse, emailResponse, id_valoracionResponse, id_rolesResponse);
                        getSupportActionBar().setSubtitle(nombresResponse + " " + apellidosResponse);
                    }

                } catch (Exception e) {
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Error: " + e, Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(array);
    }
}