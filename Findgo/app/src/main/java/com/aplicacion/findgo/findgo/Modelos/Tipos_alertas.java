package com.aplicacion.findgo.findgo.Modelos;

/**
 * Created by Nicolas on 12-12-2016.
 */

public class Tipos_alertas {

    String id_tipo_alertas;
    String nombre;

    public Tipos_alertas(String id_tipo_alerta, String nombre) {
        this.id_tipo_alertas = id_tipo_alerta;
        this.nombre = nombre;
    }

    public String getId_tipo_alertas() {
        return id_tipo_alertas;
    }

    public void setId_tipo_alertas(String id_tipo_alertas) {
        this.id_tipo_alertas = id_tipo_alertas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        //Retorna el nombre (lo que se ve en el spinner)
        return nombre;
    }

    //Para mostrarlo en el spinner ;)
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Categorias){
            Tipos_alertas t = (Tipos_alertas) obj;
            if(t.getNombre().equals(nombre) && t.getId_tipo_alertas()==id_tipo_alertas)return true;
        }
        return false;
    }
}
