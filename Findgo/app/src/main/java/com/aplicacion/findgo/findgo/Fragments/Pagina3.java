package com.aplicacion.findgo.findgo.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aplicacion.findgo.findgo.Detalle_publicacion;
import com.aplicacion.findgo.findgo.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Nicolas on 08-09-2016.
 */
public class Pagina3 extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    String latitudGlob = null;
    String longitudGlob = null;
    String titulo = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pagina3, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapViewDetalle);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        //Instanciar la clase que trae latitud y longitud de la publicacion
        Detalle_publicacion det = (Detalle_publicacion) getActivity();
        longitudGlob = det.getLongitudGlob();
        latitudGlob = det.getLatitudGlob();
        titulo = det.getTituloGlob();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                //googleMap.setMyLocationEnabled(true);

                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }
                    @Override
                    public View getInfoContents(Marker marker) {

                        Context mContext = getContext();
                        LinearLayout info = new LinearLayout(mContext);
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(mContext);
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTextSize(25);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(mContext);
                        snippet.setTextColor(Color.RED);
                        snippet.setGravity(Gravity.CENTER);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }

                });

                if((latitudGlob.equals("0") && longitudGlob.equals("0")) == false){
                    mostrarUbicacion();
                }
            }
        });

        return rootView;
    }

    private void mostrarUbicacion() {
        double latitud = 0;
        double longitud = 0;
        latitud = Double.parseDouble(latitudGlob);
        longitud = Double.parseDouble(longitudGlob);

        LatLng publicacion = new LatLng(latitud, longitud);

        googleMap.addMarker(new MarkerOptions()
                .position(publicacion)
                .title(titulo)
                .snippet("")).showInfoWindow();

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(publicacion)
                .zoom(15)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}


