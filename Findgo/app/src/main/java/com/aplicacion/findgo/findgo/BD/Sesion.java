package com.aplicacion.findgo.findgo.BD;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Nicolas on 10-11-2016.
 */

public class Sesion {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    public Sesion(Context context){
        this.context = context;
        preferences = context.getSharedPreferences("Findgo", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setLoggedin(boolean loggedin){
        editor.putBoolean("loggedInmode", loggedin);
        editor.commit();
    }
    public boolean loggein(){
        return preferences.getBoolean("loggedInmode",false);
    }
}
