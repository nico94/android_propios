package com.aplicacion.findgo.findgo.Adaptadores;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aplicacion.findgo.findgo.Fragments.MapFragment;
import com.aplicacion.findgo.findgo.Fragments.Pagina1;

/**
 * Created by Nicolas on 08-09-2016.
 */
public class Adaptador_ViewPagerPrincipal extends FragmentPagerAdapter {
    // en esta variable almacenaremos el nº de secciones
    int numeroDeSecciones;
    FloatingActionButton fab1, fab2;


    public Adaptador_ViewPagerPrincipal(FragmentManager fm, int numeroDeSecciones) {
        super(fm);
        this.numeroDeSecciones=numeroDeSecciones;
    }

    @Override
    public Fragment getItem(int position) {
        // recibimos la posición por parámetro y en función de ella devolvemos el Fragment correspondiente a esa sección.
        switch (position) {
            case 0: // siempre empieza desde 0
                return new Pagina1();
            case 1:
                return new MapFragment();
           // case 2:
             //   return new Pagina3();
            // si la posición recibida no se corresponde a ninguna sección
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numeroDeSecciones;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        // recibimos la posición por parámetro y en función de ella devolvemos el titulo correspondiente.
        switch (position) {

            case 0: // siempre empieza desde 0, la primera Tab es 0
                return "Publicaciones";
            case 1:
                return "Alertas";
            //case 2:
              //  return "Valoraciones";
            // si la posición recibida no se corresponde a ninguna sección
            default:
                return null;
        }

    }

}
