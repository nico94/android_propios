package com.aplicacion.findgo.findgo.Login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aplicacion.findgo.findgo.BD.DbHelper;
import com.aplicacion.findgo.findgo.BD.Sesion;
import com.aplicacion.findgo.findgo.MainActivity;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.Registro;
import com.aplicacion.findgo.findgo.WebServices.Constant;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;

public class login extends AppCompatActivity implements OnClickListener {

    TextView Texto;
    EditText edt_usuario, edt_password;
    TextInputLayout txt_pass_lay;
    Button btn_ingreso, btn_registrar;
    ImageView llave;
    private DbHelper db;
    private Sesion sesion;

    //Objeto Json guardado en un array
    public VolleyS volley;
    public RequestQueue requestQueue;
    JsonObjectRequest array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        View viewRoot = getWindow().getDecorView().findViewById(android.R.id.content);

        db = new DbHelper(getApplicationContext());
        sesion = new Sesion(this);

        //Buscar en vistas por ID
        edt_usuario = (EditText) findViewById(R.id.usuario);
        edt_password = (EditText) findViewById(R.id.password);
        btn_ingreso = (Button) findViewById(R.id.boton_ingreso);
        btn_registrar = (Button) findViewById(R.id.boton_registro);
        txt_pass_lay = (TextInputLayout) findViewById(R.id.lay_pass);
        llave = (ImageView) findViewById(R.id.llave);

        //Gatillar el evento OnClick
        btn_ingreso.setOnClickListener(this);
        btn_registrar.setOnClickListener(this);

        //Peticiones al ws
        volley = VolleyS.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volley.getmRequestQueue();

        VerificarInternet(viewRoot);

        //Verifica el Login
        if (sesion.loggein()) {
            startActivity(new Intent(login.this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Está seguro que quiere salir de FindGo?");
        builder.setTitle("Salir");
        builder.setIcon(R.drawable.exit);
        builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("Quedarme", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.boton_ingreso:
                login();
                break;
            case R.id.boton_registro:
                Intent intent = new Intent(login.this, Registro.class);
                startActivity(intent);
                break;
        }
    }

    public void VerificarInternet(View v) {
        //Conexion manager
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        //Verificar la conexion a internet
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            // Snackbar.make(v, "Si hay internet", Snackbar.LENGTH_LONG).show();
            //  Toast.makeText(getApplicationContext(),"Si hay conexión a internet " , Toast.LENGTH_SHORT).show();
        } else {
              Snackbar.make(v, "No hay conexión a internet", Snackbar.LENGTH_LONG)
                    .setAction("Reintentar", new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            VerificarInternet(view);
                        }
                    }).show();
        }
    }

    public void login() {
        //Rescatar string del EditText

        final ProgressDialog cargando = new ProgressDialog(this);
        cargando.setMessage("Iniciando Sesión...");
        cargando.show();

        final String usuario = edt_usuario.getText().toString();
        final String password = edt_password.getText().toString();

        //Comparar datos con variables
        if (edt_usuario.getText().length() == 0) {
            cargando.cancel();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Ingrese usuario", Snackbar.LENGTH_LONG).show();
        } else if (edt_password.getText().length() == 0) {
            cargando.cancel();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Ingrese contraseña", Snackbar.LENGTH_LONG).show();
        } else {
           array = new JsonObjectRequest(Request.Method.GET, Constant.URL_LOGIN+usuario+"/"+password, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Manejar la respuesta
                    try {
                        String verificacion = response.optString("login");
                        if(verificacion.toString().equals("Datos no válidos")){
                            cargando.cancel();
                            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                            edt_password.startAnimation(shake);
                            llave.startAnimation(shake);
                            txt_pass_lay.startAnimation(shake);

                            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),verificacion.toString() , Snackbar.LENGTH_LONG).show();
                        }else{
                            JSONArray respuesta = response.optJSONArray("login");
                            String usuarioResponse = "";
                            String passwordResponse = "";
                            String nombresResponse = "";
                            String apellidosResponse = "";
                            String telefonoResponse = "";
                            String direccionResponse = "";
                            String emailResponse = "";
                            String id_valoracionResponse = "";
                            String id_rolesResponse = "";

                            //Recorrer el arreglo Json, sacarle los strings y guardarlo en variables
                            for (int i = 0; i < respuesta.length(); i++) {
                                JSONObject jsonChildNode = respuesta.getJSONObject(i);
                                usuarioResponse = jsonChildNode.optString("usuario");
                                passwordResponse = jsonChildNode.optString("password");
                                nombresResponse = jsonChildNode.optString("nombres");
                                apellidosResponse = jsonChildNode.optString("apellidos");
                                telefonoResponse = jsonChildNode.optString("telefono");
                                direccionResponse = jsonChildNode.optString("direccion");
                                emailResponse = jsonChildNode.optString("email");
                                id_valoracionResponse = jsonChildNode.optString("id_valoracion");
                                id_rolesResponse = jsonChildNode.optString("id_roles");
                            }
                            //Ya tengo rescatado el usuario y password del json guardarlos en la BD local
                            //db.agregaUsuario(usuarioResponse,passwordResponse);
                            //Graba los datos del usuario en BD SQLite
                            byte[] data = Base64.decode(passwordResponse, Base64.DEFAULT);
                            String passDecoded = new String(data, "UTF-8");

                            db.agregaDatosUsuario(usuarioResponse, passDecoded, nombresResponse,
                                    apellidosResponse, telefonoResponse, direccionResponse, emailResponse, id_valoracionResponse, id_rolesResponse);
                            //Le digo a las SharedPreferences que el usuario esta logeado
                            sesion.setLoggedin(true);
                            Intent intent = new Intent(login.this, MainActivity.class);
                            startActivity(intent);
                            cargando.dismiss();
                            finish();
                        }


                    } catch (Exception e) {
                        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Error: " + e, Snackbar.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    cargando.cancel();
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "No hay respuesta del servidor", Snackbar.LENGTH_LONG).show();
                   // Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                    //edt_password.startAnimation(shake);
                    //llave.startAnimation(shake);
                    //txt_pass_lay.startAnimation(shake);
                }
            });
            requestQueue.add(array);
        }
    }
}
