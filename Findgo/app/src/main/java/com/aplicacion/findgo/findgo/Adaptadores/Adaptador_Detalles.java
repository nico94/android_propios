package com.aplicacion.findgo.findgo.Adaptadores;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aplicacion.findgo.findgo.Fragments.Fragment_Comentarios;
import com.aplicacion.findgo.findgo.Fragments.Fragment_Detalle_publicacion;
import com.aplicacion.findgo.findgo.Fragments.Pagina3;

/**
 * Created by Nicolas on 01-12-2016.
 */

public class Adaptador_Detalles extends FragmentPagerAdapter{
    int secciones;

    public Adaptador_Detalles (FragmentManager fm, int secciones){
        super(fm);
        this.secciones=secciones;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Fragment_Detalle_publicacion();
            case 1:
                return new Fragment_Comentarios();
            case 2:
                return new Pagina3();
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return secciones;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        // recibimos la posición por parámetro y en función de ella devolvemos el titulo correspondiente.
        switch (position) {

            case 0: // siempre empieza desde 0, la primera Tab es 0
                return "";
            case 1:
                return "";
            case 2:
                return "";
            // si la posición recibida no se corresponde a ninguna sección
            default:
                return null;
        }
    }
}
