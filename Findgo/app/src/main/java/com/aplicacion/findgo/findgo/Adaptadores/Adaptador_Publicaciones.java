package com.aplicacion.findgo.findgo.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aplicacion.findgo.findgo.Detalle_publicacion;
import com.aplicacion.findgo.findgo.Fragments.Fragment_Detalle_publicacion;
import com.aplicacion.findgo.findgo.Modelos.Publicaciones;
import com.aplicacion.findgo.findgo.R;
import com.aplicacion.findgo.findgo.WebServices.VolleyS;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nicolas on 09-11-2016.
 */

public class Adaptador_Publicaciones extends RecyclerView.Adapter<Adaptador_Publicaciones.ViewHolder> {

    private ImageLoader imageLoader;
    private Context context;
   // Animation animationFadeIn;


    //Lista de publicaciones
    List<Publicaciones> publicaciones1;

    public Adaptador_Publicaciones(List<Publicaciones> publicaciones2, Context context){
        super();
        //Tener todas las publicaciones
        this.publicaciones1 = publicaciones2;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_tarjetas, parent ,false);
        ViewHolder ViewHolder = new ViewHolder(v);
     //   animationFadeIn = (Animation) AnimationUtils.loadAnimation(parent.getContext(), R.anim.fadein);
       // v.setAnimation(animationFadeIn);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){
        final Publicaciones publicaciones = publicaciones1.get(position);
        //String imagenUrl = publicaciones.getImagen_user();
        //Picasso.with(context).load(imagenUrl).placeholder(android.R.drawable.ic_menu_camera).error(android.R.drawable.ic_dialog_alert).into(holder.imagenUsuario);

        String fechaWS = publicaciones.getFecha();
        String horaWS = publicaciones.getHora();
        String fechaFormateada = "";
        String horaFormateada = TextUtils.substring (horaWS,0,5);

        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            fechaFormateada = myFormat.format(fromUser.parse(fechaWS));
        } catch (ParseException e){
            e.printStackTrace();
        }

        imageLoader = VolleyS.getInstance(context).getImageLoader();
        imageLoader.get(publicaciones.getImagen(),imageLoader.getImageListener(holder.imagen, R.drawable.usuario_card, android.R.drawable.ic_menu_camera));

        // holder.id_post.setText("ID: "+publicaciones.getId_post());
        holder.id_categoria.setText(publicaciones.getId_categoria());
        holder.post.setText(publicaciones.getPost());
        holder.imagen.setImageUrl(publicaciones.getImagen(),imageLoader);

        holder.fecha.setText(fechaFormateada);
        holder.hora.setText("a las "+horaFormateada+" horas");
        holder.usuario.setText(publicaciones.getUsuario());
        if(publicaciones.getId_ubicacion() == "0"){
            holder.id_ubicacion.setText("No disponible");
        }else{
            holder.id_ubicacion.setText(publicaciones.getId_ubicacion());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Detalle_publicacion.class);
                intent.putExtra("id_post", publicaciones.getId_post());
                intent.putExtra("usuario", publicaciones.getUsuario());
                intent.putExtra("latitud", publicaciones.getLatitud());
                intent.putExtra("longitud", publicaciones.getLongitud());
                intent.putExtra("titulo", publicaciones.getPost());

                context.startActivity(intent);

            }
        });
    }
    @Override
    public int getItemCount(){
        return publicaciones1.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView id_categoria;
        public TextView post;
        public NetworkImageView imagen;
        public TextView fecha;
        public TextView hora;
        public TextView usuario;
        public TextView id_ubicacion;
        //public CircleImageView imagenUsuario;

        public ViewHolder(final View itemView){
            super(itemView);
            // itemView.setAnimation(animationFadeIn);
            //id_post = (TextView) itemView.findViewById(R.id.id_post_tarjeta);
            imagen = (NetworkImageView) itemView.findViewById(R.id.imagen_tarjeta);
           // imagenUsuario = (CircleImageView) itemView.findViewById(R.id.imagen_perfil);
            post = (TextView) itemView.findViewById(R.id.titulo_tarjeta);
            fecha = (TextView) itemView.findViewById(R.id.fecha_tarjeta);
            hora = (TextView) itemView.findViewById(R.id.hora_tarjeta);
            usuario = (TextView) itemView.findViewById(R.id.usuario_tarjeta);
            id_categoria = (TextView) itemView.findViewById(R.id.categoria_tarjeta);
            id_ubicacion = (TextView) itemView.findViewById(R.id.ubicacion_tarjeta);
        }
    }
}

