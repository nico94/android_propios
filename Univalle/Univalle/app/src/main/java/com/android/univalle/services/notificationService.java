package com.android.univalle.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;

import com.android.univalle.R;
import com.android.univalle.activity.MainActivity;

public class notificationService  extends Service {

    @Override
    public void onCreate() {
        checkTasks();
        super.onCreate();
    }

    private void checkTasks() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void showNotification(String mesagge){
        Intent intent = new Intent(this, MainActivity.class);
        Uri notificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap icono = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher129);
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher129)
                .setLargeIcon(icono)
                .setContentTitle(mesagge)
                .setContentText("Prueba")
                .setSound(notificacion)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        notificationBuilder.setStyle(new Notification.BigTextStyle(notificationBuilder)
                .bigText(mesagge)
                .setSummaryText("Resumen notificacion")
        );

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(0, notificationBuilder.build());

    }

}
