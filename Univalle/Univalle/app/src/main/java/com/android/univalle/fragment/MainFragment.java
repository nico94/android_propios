package com.android.univalle.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.android.univalle.R;
import com.android.univalle.activity.CategoryActivity;
import com.android.univalle.activity.DeviceListActivity;
import com.android.univalle.adapter.CategoryAdapter;
import com.android.univalle.adapter.template.ModelAdapter;
import com.android.univalle.bluetooth.BluetoothService;
import com.android.univalle.bluetooth.Constants;
import com.android.univalle.db.Controller;
import com.android.univalle.db.OpenHelper;
import com.android.univalle.fragment.template.RecyclerFragment;
import com.android.univalle.model.Category;
import com.android.univalle.model.DatabaseModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainFragment extends RecyclerFragment<Category, CategoryAdapter> {
	private static final int REQUEST_CONNECT_DEVICE_SECURE = 10;
	private static final int REQUEST_ENABLE_BT = 11;

	private int categoryDialogTheme = Category.THEME_GREEN;


	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothService mChatService = null;
	private String mConnectedDeviceName = null;


	private ModelAdapter.ClickListener listener = new ModelAdapter.ClickListener() {
		@Override
		public void onClick(DatabaseModel item, int position) {
			Intent intent = new Intent(getContext(), CategoryActivity.class);
			intent.putExtra("position", position);
			intent.putExtra(OpenHelper.COLUMN_ID, item.id);
			intent.putExtra(OpenHelper.COLUMN_TITLE, item.title);
			intent.putExtra(OpenHelper.COLUMN_THEME, ((Category) item).theme);
			startActivityForResult(intent, CategoryActivity.REQUEST_CODE);
		}

		@Override
		public void onChangeSelection(boolean haveSelected) {
			toggleSelection(haveSelected);
		}

		@Override
		public void onCountSelection(int count) {
			onChangeCounter(count);
			activity.toggleOneSelection(count <= 1);
		}
	};

	public MainFragment(){}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			FragmentActivity activity = getActivity();
			Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			activity.finish();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else if (mChatService == null) {
			setupChat();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mChatService != null) {
			mChatService.stop();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mChatService != null) {
			if (mChatService.getState() == BluetoothService.STATE_NONE) {
				mChatService.start();
			}
		}
	}

	@Override
	public void onClickFab() {
		categoryDialogTheme = Category.THEME_GREEN;
		displayCategoryDialog(
			R.string.new_category,
			R.string.create,
			"",
			DatabaseModel.NEW_MODEL_ID,
			0
		);
	}

	public void onEditSelected() {
		if (!selected.isEmpty()) {
			Category item = selected.remove(0);
			int position = items.indexOf(item);
			refreshItem(position);
			toggleSelection(false);
			categoryDialogTheme = item.theme;
			displayCategoryDialog(
				R.string.edit_category,
				R.string.edit,
				item.title,
				item.id,
				position
			);
		}
	}

	public void onShareSelected() {
		if (!selected.isEmpty()) {
			Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
		}
	}

	private void displayCategoryDialog(@StringRes int title, @StringRes int positiveText, final String categoryTitle, final long categoryId, final int position) {
		MaterialDialog dialog = new MaterialDialog.Builder(getContext())
			.title(title)
			.positiveText(positiveText)
			.negativeText(R.string.cancel)
			.negativeColor(ContextCompat.getColor(getContext(), R.color.secondary_text))
			.onPositive(new MaterialDialog.SingleButtonCallback() {
				@Override
				public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
					//noinspection ConstantConditions
					String inputTitle = ((EditText) dialog.getCustomView().findViewById(R.id.title_txt)).getText().toString();
					if (inputTitle.isEmpty()) {
						inputTitle = "Untitled";
					}

					final Category category = new Category();
					category.id = categoryId;

					final boolean isEditing = categoryId != DatabaseModel.NEW_MODEL_ID;

					if (!isEditing) {
						category.counter = 0;
						category.type = DatabaseModel.TYPE_CATEGORY;
						category.createdAt = System.currentTimeMillis();
						category.isArchived = false;
					}

					category.title = inputTitle;
					category.theme = categoryDialogTheme;

					new Thread() {
						@Override
						public void run() {
							final long id = category.save();
							if (id != DatabaseModel.NEW_MODEL_ID) {
								getActivity().runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (isEditing) {
											Category categoryInItems = items.get(position);
											categoryInItems.theme = category.theme;
											categoryInItems.title = category.title;
											refreshItem(position);
										} else {
											category.id = id;
											addItem(category, position);
										}
									}
								});
							}

							interrupt();
						}
					}.start();

				}
			})
			.onNegative(new MaterialDialog.SingleButtonCallback() {
				@Override
				public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
					dialog.dismiss();
				}
			})
			.customView(R.layout.dialog_category, true)
			.build();

		dialog.show();

		final View view = dialog.getCustomView();

		//noinspection ConstantConditions
		((EditText) view.findViewById(R.id.title_txt)).setText(categoryTitle);
		setCategoryDialogTheme(view, categoryDialogTheme);

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_red).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_RED);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_pink).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_PINK);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_purple).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_PURPLE);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_amber).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_AMBER);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_blue).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_BLUE);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_cyan).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_CYAN);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_orange).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_ORANGE);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_teal).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_TEAL);
			}
		});

		//noinspection ConstantConditions
		view.findViewById(R.id.theme_green).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setCategoryDialogTheme(view, Category.THEME_GREEN);
			}
		});
	}

	private void setCategoryDialogTheme(View view, int theme) {
		if (theme != categoryDialogTheme) {
			getThemeView(view, categoryDialogTheme).setImageResource(0);
		}

		getThemeView(view, theme).setImageResource(R.drawable.ic_checked);
		categoryDialogTheme = theme;
	}

	private ImageView getThemeView(View view, int theme) {
		switch (theme) {
			case Category.THEME_AMBER:
				return (ImageView) view.findViewById(R.id.theme_amber);
			case Category.THEME_BLUE:
				return (ImageView) view.findViewById(R.id.theme_blue);
			case Category.THEME_CYAN:
				return (ImageView) view.findViewById(R.id.theme_cyan);
			case Category.THEME_ORANGE:
				return (ImageView) view.findViewById(R.id.theme_orange);
			case Category.THEME_PINK:
				return (ImageView) view.findViewById(R.id.theme_pink);
			case Category.THEME_PURPLE:
				return (ImageView) view.findViewById(R.id.theme_purple);
			case Category.THEME_RED:
				return (ImageView) view.findViewById(R.id.theme_red);
			case Category.THEME_TEAL:
				return (ImageView) view.findViewById(R.id.theme_teal);
			default:
				return (ImageView) view.findViewById(R.id.theme_green);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CategoryActivity.REQUEST_CODE && resultCode == CategoryActivity.RESULT_CHANGE) {
			int position = data.getIntExtra("position", 0);
			items.get(position).counter = data.getIntExtra(OpenHelper.COLUMN_COUNTER, 0);
			refreshItem(position);
		}

		if (requestCode == REQUEST_CONNECT_DEVICE_SECURE && resultCode == Activity.RESULT_OK) {
			connectDevice(data, true);
		}
	}

	@Override
	public int getLayout() {
		return (R.layout.fragment_main);
	}

	@Override
	public String getItemName() {
		return "category";
	}

	@Override
	public Class<CategoryAdapter> getAdapterClass() {
		return CategoryAdapter.class;
	}

	@Override
	public ModelAdapter.ClickListener getListener() {
		return listener;
	}

	private void connectDevice(Intent data, boolean secure) {
		// Get the device MAC address
		String address = data.getExtras()
				.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
		// Get the BluetoothDevice object
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		// Attempt to connect to the device
		mChatService.connect(device, secure);
	}

	private void disconnectDevice() {
		mChatService.start();
}

	private void setupChat() {
	// Initialize the BluetoothChatService to perform bluetooth connections
		mChatService = new BluetoothService(getActivity(), mHandler);
	}

	private void sendMessageToTarget(String message) {
		// Check that we're actually connected before trying anything
		if (mChatService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothChatService to write
			byte[] send = message.getBytes();
			mChatService.write(send);
		}
	}

	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			FragmentActivity activity = getActivity();
			switch (msg.what) {
				case Constants.MESSAGE_STATE_CHANGE:
					break;
				case Constants.MESSAGE_WRITE:
					disconnectDevice();
				break;
				case Constants.MESSAGE_READ:
					byte[] readBuf = (byte[]) msg.obj;
					// construct a string from the valid bytes in the buffer
					String readMessage = new String(readBuf, 0, msg.arg1);
					try {
						JSONArray json = new JSONArray(readMessage);
						Controller.instance.readBackup(json);
						loadItems();
					} catch (Exception ignored) {

					}
					disconnectDevice();
					break;
				case Constants.MESSAGE_DEVICE_NAME:
					// save the connected device's name
					mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
					if (null != activity) {
						Toast.makeText(activity, "Connected to "
								+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
					}

					boolean isMaster = msg.getData().getBoolean(Constants.IS_MASTER);

					if (isMaster) {
						try {
							ArrayList<String> ids = new ArrayList<>();
							for (Category category: selected) {
								ids.add(String.valueOf(category.id));
							}
							sendMessageToTarget(Controller.instance.getBackupString(ids));
						} catch (JSONException ignored) {

						}
					}

					break;
				case Constants.MESSAGE_TOAST:
					if (null != activity) {
						Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
								Toast.LENGTH_SHORT).show();
					}
					break;
			}
		}
	};
}
