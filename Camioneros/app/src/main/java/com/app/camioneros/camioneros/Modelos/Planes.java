package com.app.camioneros.camioneros.Modelos;

/**
 * Created by Nicolas on 02-10-2017.
 */

public class Planes {
    String key,nombre, fecha;

    public Planes() {
    }

    public Planes(String key, String nombre, String fecha) {
        this.key = key;
        this.nombre = nombre;
        this.fecha = fecha;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}