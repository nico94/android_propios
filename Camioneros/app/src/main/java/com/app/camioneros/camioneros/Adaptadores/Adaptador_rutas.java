package com.app.camioneros.camioneros.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.camioneros.camioneros.Modelos.Rutas;
import com.app.camioneros.camioneros.R;

import java.util.List;

/**
 * Created by Nicolas on 13-10-2017.
 */

public class Adaptador_rutas extends RecyclerView.Adapter<Adaptador_rutas.RutasViewHolder> {

    private List<Rutas> list;

    public Adaptador_rutas(List<Rutas> list){
        this.list=list;
    }

    @Override
    public RutasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RutasViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tarjeta_ruta,parent,false));
    }

    @Override
    public void onBindViewHolder(RutasViewHolder holder, int position) {
        final Rutas rutas = list.get(position);
        holder.txt_descripcionRuta.setText(rutas.getDescripcion());
        holder.txt_estadoRuta.setText(rutas.getEstado());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

     class RutasViewHolder extends RecyclerView.ViewHolder{

         TextView txt_descripcionRuta, txt_estadoRuta;

         public RutasViewHolder(View itemView){
             super(itemView);
             txt_descripcionRuta = (TextView) itemView.findViewById(R.id.txt_descripcionRuta);
             txt_estadoRuta = (TextView) itemView.findViewById(R.id.txt_estadoRuta);
         }
    }
}