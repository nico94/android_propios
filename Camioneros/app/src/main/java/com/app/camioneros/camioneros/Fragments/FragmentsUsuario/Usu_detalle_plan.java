package com.app.camioneros.camioneros.Fragments.FragmentsUsuario;


import android.os.Bundle;
import android.os.health.SystemHealthManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.camioneros.camioneros.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Usu_detalle_plan extends Fragment {
    TextView txt_detallePlan, txt_detalleVehiculo, txt_nombreChofer, txt_direcciones, txt_kilometros,txt_volumen, txt_ordenes, txt_tiempoEstimado, txt_fechaPlan;
    Button btn_continuar;
    Bundle datos;
    String usuarioLogueado,key_plan,detalle_plan,fecha_plan,vehiculo ;
    String nombreChofer;
    int cantUbicaciones, cantKilometros;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    FirebaseDatabase database;
    DatabaseReference refPlan,refUsuario,refRutas;


    public Usu_detalle_plan() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_usu_detalle_plan,container,false);

        datos = this.getArguments();
        key_plan = datos.getString("key_plan");

        //Inicializaciones
        database = FirebaseDatabase.getInstance();
        refPlan = database.getReference().child("planes").child(key_plan);
        refUsuario = database.getReference().child("usuarios");
        refRutas = database.getReference().child("rutas");

        //Inicializar componentes
        txt_detallePlan=(TextView) v.findViewById(R.id.txt_detallePlan);
        txt_detalleVehiculo=(TextView) v.findViewById(R.id.txt_detalleVehiculo);
        txt_nombreChofer = (TextView) v.findViewById(R.id.txt_nombreChofer);
        txt_direcciones = (TextView) v.findViewById(R.id.txt_direccionesDet);
        txt_kilometros = (TextView) v.findViewById(R.id.txt_kilometrosDet);
        txt_volumen = (TextView) v.findViewById(R.id.txt_volumenDet);
        txt_ordenes = (TextView) v.findViewById(R.id.txt_ordenesDet);
        txt_tiempoEstimado = (TextView) v.findViewById(R.id.txt_tiempoDet);
        txt_fechaPlan = (TextView) v.findViewById(R.id.txt_fechaDet);
        btn_continuar = (Button) v.findViewById(R.id.btn_continuarDet);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        usuarioLogueado=user.getUid().toString();
        
        clickContinuar();
        datosUsuario();
        obtenerDatosPlan();
        cantidadRutas();

        return v;
    }

    private void clickContinuar() {
        btn_continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(key_plan!=null){
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    Fragment usu_rutas = new Usu_rutas();
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    Bundle datos = new Bundle();
                    datos.putString("key_plan",key_plan);
                    usu_rutas.setArguments(datos);
                    transaction.replace(R.id.contenedor_fragments,usu_rutas);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
    }

    private void actualizarInterfaz() {
        //Validar los datos xd
        txt_detallePlan.setText(detalle_plan);
        txt_nombreChofer.setText(nombreChofer);
        txt_direcciones.setText("#Ubicaciones "+cantUbicaciones);
        txt_kilometros.setText("#Kilometros "+cantKilometros);
        txt_fechaPlan.setText(fecha_plan);
    }

    private void datosUsuario(){
        refUsuario.child(usuarioLogueado).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                nombreChofer=String.valueOf(dataSnapshot.child("nombre").getValue());
                actualizarInterfaz();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void cantidadRutas(){
        refPlan.child("rutas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                cantUbicaciones=0;
                for(DataSnapshot rutasSnapShot : dataSnapshot.getChildren()){
                    cantUbicaciones++;
                    actualizarInterfaz();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void obtenerDatosPlan() {
        refPlan.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                detalle_plan=dataSnapshot.child("nombre").getValue().toString();
                fecha_plan=dataSnapshot.child("fecha").getValue().toString();
                for (DataSnapshot planesSnapshot : dataSnapshot.getChildren()) {
                    //int size = planesSnapshot.getChildrenCount();
                }
                actualizarInterfaz();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}