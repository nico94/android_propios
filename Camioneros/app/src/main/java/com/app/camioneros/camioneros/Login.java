package com.app.camioneros.camioneros;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity implements OnClickListener{
    //Declarar componentes
    private EditText edt_correo, edt_password;
    private Button btn_ingresar, btn_registrar;
    TextInputLayout input_correo;
    private ProgressDialog barraProgreso;
    // Declarar autentificacion de Usuario de firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Rescatar la vista de root
        View viewRoot = getWindow().getDecorView().findViewById(android.R.id.content);

        mAuth = FirebaseAuth.getInstance();

//        if (mAuth.getCurrentUser() != null) {
//            Intent intent = new Intent(Login.this, Principal.class);
//            startActivity(intent);
//            finish();
//        }

        if(getSupportActionBar() != null)
            getSupportActionBar().hide();
        //Buscar componentes por ID
        edt_correo = (EditText) findViewById(R.id.edt_correo);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btn_ingresar = (Button) findViewById(R.id.btn_ingreso);
        btn_registrar = (Button) findViewById(R.id.btn_registro);
        input_correo = (TextInputLayout) findViewById(R.id.input_correoLogin);
       // edt_correo.addTextChangedListener(new visorEdt(edt_correo));
        //Setearle las funciones a los componentes
        btn_ingresar.setOnClickListener(this);
        btn_registrar.setOnClickListener(this);
        barraProgreso = new ProgressDialog(this);

//
//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                if (firebaseAuth.getCurrentUser() != null) {
//                    Intent intent = new Intent(Login.this, Principal.class);
//                    startActivity(intent);
//                    finish();
//                }
//            }
//        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ingreso:
                login();
                break;
            case R.id.btn_registro:
                Intent intent = new Intent(Login.this, Registro.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //Varifica si el suario esta logueado y actualiza la vista.
        //mAuth.addAuthStateListener(mAuthListener); //Asi estaba
        if(mAuth.getCurrentUser() != null){
            onAuthSuccess(mAuth.getCurrentUser());
        }
    }

    private void onAuthSuccess(FirebaseUser usuario) {
//        String userName = usernameFromEmail(usuario.getEmail());
//        nuevoUsuario(usuario.getUid(),usuario,usuario.getEmail());
        startActivity(new Intent(Login.this, Principal.class));
        finish();
    }

    public void login(){
        final String correo = edt_correo.getText().toString().trim();
        final String password = edt_password.getText().toString().trim();

        if (TextUtils.isEmpty(correo)) {
            barraProgreso.dismiss();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Ingrese correo", Snackbar.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(password)) {
            barraProgreso.dismiss();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Ingrese contraseña", Snackbar.LENGTH_LONG).show();
        } else {
            barraProgreso.setMessage("Iniciando sesión, por favor espere...");
            barraProgreso.show();
            mAuth.signInWithEmailAndPassword(correo, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            barraProgreso.dismiss();
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Sesión iniciada correctamente", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Login.this, Principal.class);
                                startActivity(intent);
                                finish();
                            } else
//                                Animation shake= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.shake);
//                                edt_password.startAnimation(shake);
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
        }
    }

    private class visorEdt implements TextWatcher {
        private View view;
        private visorEdt(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void afterTextChanged(Editable editable) {
            validarCorreo();
        }
    }

    private boolean validarCorreo() {
        String email = edt_correo.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            input_correo.setError("Ingrese un email válido");
            //requestFocus(input_email);
            return false;
        } else {
            input_correo.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    protected void onStop(){
        super.onStop();
        if(mAuth.getCurrentUser() != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}