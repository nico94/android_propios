package com.app.camioneros.camioneros.Fragments.FragmentsUsuario;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.camioneros.camioneros.Adaptadores.Adaptador_rutas;
import com.app.camioneros.camioneros.Modelos.Rutas;
import com.app.camioneros.camioneros.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Usu_rutas extends Fragment {
    private RecyclerView recyclerView;
    public List<Rutas> rutasList;
    private Adaptador_rutas adaptador_rutas;
    FirebaseDatabase database;
    DatabaseReference refPlan, refRutas;
    Bundle datos;
    String plan;

    public Usu_rutas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_usu_rutas, container, false);

        database = FirebaseDatabase.getInstance();
        refPlan = database.getReference().child("planes");
        refRutas = database.getReference().child("rutas");

        rutasList = new ArrayList<>();

        datos = this.getArguments();
        plan = datos.getString("key_plan");

        recyclerView = (RecyclerView) viewRoot.findViewById(R.id.reciclador_rutas);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);
        actualizarVista();

        adaptador_rutas = new Adaptador_rutas(rutasList);
        recyclerView.setAdapter(adaptador_rutas);

        return viewRoot;
    }

    private void actualizarVista() {
        refPlan.child(plan).child("rutas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot rutasSnapshot: dataSnapshot.getChildren()){
                    if(dataSnapshot.child(rutasSnapshot.getKey()).exists()){
                        String ruta = rutasSnapshot.getKey().toString();
                        buscarRutas(ruta);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void buscarRutas(final String key_ruta){
        if(key_ruta!=null){
            refRutas.child(key_ruta).addValueEventListener(new ValueEventListener() {
//            refRutas.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
             //       rutasList.clear();
                    Toast.makeText(getContext(), dataSnapshot.getKey().toString(), Toast.LENGTH_SHORT).show();
//                    if(dataSnapshot.getKey().toString().equals(key_ruta)){
                        Rutas rutas = dataSnapshot.getValue(Rutas.class);
                        rutas.setKey(key_ruta);
//                        rutas.setEstado(String.valueOf(dataSnapshot.child("estado").getValue());
                        rutasList.add(rutas);
//                    }
                adaptador_rutas = new Adaptador_rutas(rutasList);
                recyclerView.setAdapter(adaptador_rutas);

                //String key = String.valueOf(dataSnapshot.getKey().toString());
                //rutasList = new ArrayList<>();
                }
                @Override
                public void onCancelled(DatabaseError databaseError){

                }
            });
        }
    }
}