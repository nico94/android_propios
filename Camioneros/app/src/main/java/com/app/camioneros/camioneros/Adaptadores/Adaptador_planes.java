package com.app.camioneros.camioneros.Adaptadores;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.camioneros.camioneros.Fragments.Administrador;
import com.app.camioneros.camioneros.Fragments.FragmentsUsuario.Usu_detalle_plan;
import com.app.camioneros.camioneros.Modelos.Planes;
import com.app.camioneros.camioneros.Principal;
import com.app.camioneros.camioneros.R;

import java.util.List;

/**
 * Created by Nicolas on 04-10-2017.
 */

public class Adaptador_planes extends  RecyclerView.Adapter<Adaptador_planes.PlanViewHolder>{

    private List<Planes> list;
    private Context context;
    public Adaptador_planes(List<Planes> list) {
        this.list = list;
    }

    @Override
    public PlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlanViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tarjeta_plan,parent,false));
    }

    @Override
    public void onBindViewHolder(final PlanViewHolder holder, int position) {
        final Planes planes = list.get(position);
        holder.txt_nombre.setText(planes.getNombre());
        holder.txt_fecha.setText(planes.getFecha());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String planSeleccionado = planes.getKey();
                if(planSeleccionado!=null){
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    Fragment detalle_plan = new Usu_detalle_plan();
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    Bundle datos = new Bundle();
                    datos.putString("key_plan",planSeleccionado);
                    detalle_plan.setArguments(datos);
                    transaction.replace(R.id.contenedor_fragments,detalle_plan);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PlanViewHolder extends RecyclerView.ViewHolder{

        TextView txt_nombre, txt_fecha;

        public PlanViewHolder(View itemView) {
            super(itemView);
            txt_nombre = (TextView) itemView.findViewById(R.id.txt_nombrePlan);
            txt_fecha = (TextView) itemView.findViewById(R.id.txt_fechaPlan);
        }
    }
}