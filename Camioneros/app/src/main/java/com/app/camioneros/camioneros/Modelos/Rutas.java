package com.app.camioneros.camioneros.Modelos;

/**
 * Created by Nicolas on 02-10-2017.
 */

public class Rutas {
    String key, descripcion, estado ;
    long latitud_fin,longitud_fin,latitud_inicio,longitud_inicio;

    public Rutas() {

    }

    public Rutas(String key, String descripcion, String estado, long latitud_fin, long longitud_fin, long latitud_inicio, long longitud_inicio) {
        this.key = key;
        this.descripcion = descripcion;
        this.estado = estado;
        this.latitud_fin = latitud_fin;
        this.longitud_fin = longitud_fin;
        this.latitud_inicio = latitud_inicio;
        this.longitud_inicio = longitud_inicio;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getLatitud_fin() {
        return latitud_fin;
    }

    public void setLatitud_fin(long latitud_fin) {
        this.latitud_fin = latitud_fin;
    }

    public long getLongitud_fin() {
        return longitud_fin;
    }

    public void setLongitud_fin(long longitud_fin) {
        this.longitud_fin = longitud_fin;
    }

    public long getLatitud_inicio() {
        return latitud_inicio;
    }

    public void setLatitud_inicio(long latitud_inicio) {
        this.latitud_inicio = latitud_inicio;
    }

    public long getLongitud_inicio() {
        return longitud_inicio;
    }

    public void setLongitud_inicio(long longitud_inicio) {
        this.longitud_inicio = longitud_inicio;
    }
}
