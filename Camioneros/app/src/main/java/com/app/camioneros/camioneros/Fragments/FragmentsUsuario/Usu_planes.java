package com.app.camioneros.camioneros.Fragments.FragmentsUsuario;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.camioneros.camioneros.Adaptadores.Adaptador_planes;
import com.app.camioneros.camioneros.Modelos.Planes;
import com.app.camioneros.camioneros.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Usu_planes extends Fragment {

    private RecyclerView recyclerView;
    public List<Planes> planesList;
    private Adaptador_planes adaptador;
    FirebaseDatabase database;
    DatabaseReference refPlan;
    Bundle datos;
    private String usuario;

    public Usu_planes() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_usu_planes, container, false);

        database = FirebaseDatabase.getInstance();
        refPlan = database.getReference().child("planes");

        planesList = new ArrayList<>();

        datos = this.getArguments();
        usuario = datos.getString("Usuario");

        recyclerView = (RecyclerView) v.findViewById(R.id.reciclador_planes);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);
        actualizarLista();

        adaptador = new Adaptador_planes(planesList);
        recyclerView.setAdapter(adaptador);

        return v;
    }

    private void actualizarLista() {
        refPlan.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                planesList.clear();
                for (DataSnapshot planesSnapshot : dataSnapshot.getChildren()) {
                    if (planesSnapshot.child("usuarios/" + usuario).exists()) {
                        String key = String.valueOf(planesSnapshot.getKey().toString());
                        Planes planes1 = planesSnapshot.getValue(Planes.class);
                        planes1.setKey(key);
                        planesList.add(planes1);
                    }
                }
                adaptador = new Adaptador_planes(planesList);
                recyclerView.setAdapter(adaptador);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

//    private void actualizarLista(){
//        refPlan.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                planesList.add(dataSnapshot.getValue(Planes.class));
//                adaptador.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Planes planes1 = dataSnapshot.getValue(Planes.class);
//                planesList.add(planes1);
//                //planesList.add(dataSnapshot.getValue(Planes.class));
//                adaptador.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Planes planes = dataSnapshot.getValue(Planes.class);
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
