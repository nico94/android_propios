package com.app.camioneros.camioneros;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Registro extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private EditText edt_nombre, edt_correo, edt_password1,edt_password2;
    private TextInputLayout inputEmail;
    private Button btn_registrarse;
    private ProgressDialog barraProgreso;
    private FirebaseAuth.AuthStateListener mAuthListener;
    View viewRoot;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        //Rescatar la vista Root de la Actividad
        viewRoot = getWindow().getDecorView().findViewById(android.R.id.content);
        //Quitar la barra de la app
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        //Obtener la isntancia a la autentificacion de Firebase
        mAuth = FirebaseAuth.getInstance();
        //Buscar los id en la vista
        edt_nombre = (EditText) findViewById(R.id.edt_nombreR);
        edt_correo = (EditText) findViewById(R.id.edt_correoR);
        edt_password1 = (EditText) findViewById(R.id.edt_passwordR1);
        edt_password2 = (EditText) findViewById(R.id.edt_passwordR2);
        btn_registrarse = (Button) findViewById(R.id.btn_registro1);
        inputEmail = (TextInputLayout) findViewById(R.id.input_emailRegistro);
        //Setear los callbacks
        edt_correo.addTextChangedListener(new MiVisor(edt_correo));
        btn_registrarse.setOnClickListener(this);
        barraProgreso = new ProgressDialog(this);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Si el Usuario es distinto de vacio
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent intent = new Intent(Registro.this, Login.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
    }

    @Override
    public void onClick(View v){
        //Llamo mi metodo de iniciar Registro
        iniciarRegistro();
    }

    public void iniciarRegistro(){
        //Guardo en variables los editText
        final String nombre = edt_nombre.getText().toString().trim();
        final String correo = edt_correo.getText().toString().trim();
        final String password = edt_password1.getText().toString().trim();
        final String password1 = edt_password2.getText().toString().trim();
        //Valido si no vienen vacios
        if(TextUtils.isEmpty(nombre)){
            barraProgreso.dismiss();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Nombre requerido", Snackbar.LENGTH_LONG).show();
        }else if (TextUtils.isEmpty(correo)){
            barraProgreso.dismiss();
            Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Correo requerido", Snackbar.LENGTH_LONG).show();
        }else{
            if(password.equalsIgnoreCase(password1)){
                //Aqui hacer el Registro
                barraProgreso.setMessage("Haciendo Registro, por favor espere...");
                barraProgreso.show();
                //Le paso el correo y password a la funcion para regustrar en firebase
                mAuth.createUserWithEmailAndPassword(correo, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                barraProgreso.dismiss();
                                //Si se completa el Registro
                                if (task.isSuccessful()) {
                                    //Loguearse con el correo y password guardadas
                                    mAuth.signInWithEmailAndPassword(correo, password);
                                    Toast.makeText(getApplicationContext(),"Usuario registrado correctamente",Toast.LENGTH_LONG).show();
                                    //Hacer una instancia a la base de datos de Firebase
                                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("usuarios");
                                    DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
                                    currentUserDB.child("nombre").setValue(nombre);
                                    currentUserDB.child("perfil").setValue(2);
                                    currentUserDB.child("imagen").setValue("default");
                                }else{
                                    Snackbar.make(viewRoot, "Error al registrar suario", Snackbar.LENGTH_LONG).show();
                                }
                            }
                        });
            }else{
                barraProgreso.dismiss();
                Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Las contraseñas no coinciden", Snackbar.LENGTH_LONG).show();
            }
        }
    }


    private class MiVisor implements TextWatcher {
        private View view;

        private MiVisor(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edt_correo:
                    validarCorreo();
                    break;
            }
        }
    }
    private boolean validarCorreo() {
        String email = edt_correo.getText().toString().trim();
        if (email.isEmpty() || !correoValido(email)) {
            inputEmail.setError("Ingrese un correo válido");
            //requestFocus(input_email);
            return false;
        } else {
            inputEmail.setErrorEnabled(false);
        }
        return true;
    }
    private static boolean correoValido(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}