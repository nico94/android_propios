package com.app.camioneros.camioneros;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.app.camioneros.camioneros.Fragments.Administrador;
import com.app.camioneros.camioneros.Fragments.FragmentsUsuario.Usu_detalle_plan;
import com.app.camioneros.camioneros.Fragments.FragmentsUsuario.Usu_planes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

public class Principal extends AppCompatActivity {
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference refUsuario, refPlan;
    private ProgressDialog barraProgreso;
    private String usuarioLogueado;
    Bundle mSavedInstanceState;
    String usuario, perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        View viewRoot = getWindow().getDecorView().findViewById(android.R.id.content);
        mAuth = FirebaseAuth.getInstance();
        //Referencias a la base de datos
        refUsuario=FirebaseDatabase.getInstance().getReference().child("usuarios");
        refPlan=FirebaseDatabase.getInstance().getReference().child("planes");

        //Buscar porID
        toolbar = (Toolbar) findViewById(R.id.ToolbarPrincipal);
        appBarLayout = (AppBarLayout) findViewById(R.id.AppbarPrincipal);

        //Setear los callbacks
        barraProgreso = new ProgressDialog(this);
        usuarioLogueado=mAuth.getCurrentUser().getUid().toString();

        System.out.println("usuario logueado "+usuarioLogueado);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    usuarioLogueado=firebaseAuth.getCurrentUser().getUid();
                 }else{
                    startActivity(new Intent(Principal.this, Login.class));
                    finish();
                }
            }
        };

        if(usuarioLogueado!=null){
            refUsuario.child(usuarioLogueado).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    barraProgreso.setMessage("Cargando perfil...");
                    barraProgreso.show();
                    usuario=String.valueOf(dataSnapshot.child("nombre").getValue());
                    perfil=String.valueOf(dataSnapshot.child("perfil").getValue());
                    actualizarVista();
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

        //Inicializar toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Bienvenido");
    }

    public void actualizarVista(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(mSavedInstanceState == null){
            if(usuario != null && perfil !=null){
                getSupportActionBar().setSubtitle(usuario);
                barraProgreso.dismiss();
                if (perfil.equalsIgnoreCase("1")){ //Administradores
                    Administrador adm = new Administrador();
                    //FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.contenedor_fragments,adm);
                    transaction.commit();
                }else if (perfil.equalsIgnoreCase("2")){ //usuarios
                    Usu_planes usu_planes = new Usu_planes();
                    Bundle datos = new Bundle();
                    datos.putString("Usuario",usuarioLogueado);
                    usu_planes.setArguments(datos);
                    transaction.replace(R.id.contenedor_fragments,usu_planes);
                    transaction.commit();
                }
            }else{
                barraProgreso.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
            case R.id.perfil:

                return true;
            case R.id.cerrar_sesion:
                //Aqui lanzar una advertencia Yes/no
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("¿Está seguro que quiere cerrar sesión?");
                builder.setTitle("Cerrar sesión");
                //builder.setIcon(R.drawable.puerta);
                builder.setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Logout
                        mAuth.signOut();
                        finish();
                   }
                });
                builder.setNegativeButton("Quedarme", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialogSesion = builder.create();
                dialogSesion.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop(){
        super.onStop();
    }
}